一　大逃殺

我們等大家都睡醒，便按鈴呼叫米蕾依小姐，請她將早餐送到房間。主要的餐點是用鳥類魔物『甜蜜鳥』的蛋烹調的料理，上面還擺了同種魔物的燻肉。兩種料理都相當好吃，很對我們的胃口。

沙拉則使用名為『子鬼草』的植物系魔物莖部，風味和口感都類似西洋菜，和酸甜的水果醬汁搭配得宜，嘗起來也很不錯。子鬼草的根部長得像是哥布林的臉，但只要忽略這一點，據說它的口感就像蕪菁一樣美味──不過這次並沒有出現在菜色內。

我們享用完早餐就做好了外出的準備。包括米蕾依小姐在內的所有女僕都在玄關列隊目送我們離開──這讓我感覺自己好像是這棟宅邸的主人。

「我們出發了。」
「請問各位大約什麼時候會回來呢？只要大概的時間就可以了。」
「我想想喔，傍晚六點應該能回來。」
「好的。我會配合各位的時間準備晚餐。路上請小心。」
「『『『路上請小心！』』』」

女僕的總人數多達十位，她們整齊劃一地同時道別，讓我們不禁為之震懾。我想起以前為了見見世面造訪女僕咖啡廳的事──不過這個世界的女僕更有專業氣息。

我們將盧恩石和礦石送往冶煉廠前先順路去了公會一趟，發現布告欄貼出一張用紅字寫了些什麼的紙，探索者則聚集在布告欄前七嘴八舌地談論。

「要是對大逃殺置之不理，魔物甚至會跑到外面吧？」
「午睡濕地嗎⋯⋯還真是不會挑地點啊。大家都對那裡敬而遠之，所以才會放置不管。」

我從人群擠出確認布告的內容。

──由於『午睡濕地』的魔物討伐數量銳減，目前已列入大逃殺警戒區域。敬請等級３以上的各位探索者協助討伐魔物，酬勞及貢獻度將較平時增加３成。　負責人：八號區公會本部長

（露易莎小姐⋯⋯看來她遇到棘手的狀況了呢。）

如果這是公會貼出的布告，我也希望能盡量出力。測驗是在明天舉行，就算今天前往『午睡濕地』也沒問題。

「有人，你打算怎麼做？」

艾莉緹亞向我問道。其他人似乎也對大逃殺不甚瞭解，我們應該先從掌握情報開始。

「艾莉緹亞，大逃殺具體來說會發生什麼事？」
「迷宮的魔物有時候會進行生物上的繁衍，除此之外，迷宮本身也會為了維持內部的環境『產生』魔物。要是魔物的數量變得太多，迷宮就會強制將魔物從一樓入口送到外面，魔物異常增加的狀態就被稱為『大逃殺』。」

迷宮入口乍看之下只是一座樓梯，但我知道走到一半就會被轉移到其他地方。然而，我沒想到魔物竟然會從迷宮裡跑出來。

「聽起來迷宮簡直像是有生命⋯⋯還能夠以自己的意志思考。」

五十嵐小姐的話讓我想起了阿里阿德涅。第一百一十七號秘神──這意味著秘神共有一百尊以上。

（⋯⋯迷宮的隱藏樓層和其他樓層完全不同，因此能合理假設沉睡於該處的秘神和迷宮擁有密切的關連。也就是說，祂們即為迷宮意志的代理人⋯⋯不，阿里阿德涅並沒有展現這樣的舉止。）

但假設所有迷宮都有秘神所在的隱藏樓層──那麼透過發現阿里阿德涅以外的秘神，或許能掌握迷宮之所以存在的線索。

「後部大人，您早。驚動到您真是不好意思。」
「早啊，露易莎小姐。事情好像滿嚴重的呢。能不能也讓我們協助處理大逃殺的問題呢？」

露易莎小姐聽完睜大了眼睛。雖然愈來愈常讓她驚訝，但我認為提出這樣的申請是理所當然的。

「很感謝您如此關心。對我來說⋯⋯不，目前隸屬於八號區公會的隊伍中，屬各位的實力最為堅強。請務必助我們一臂之力。」
「露易莎小姐⋯⋯您完全不需要客氣，畢竟緊急時刻互相幫助本來就是應該的。」

露易莎小姐用手帕擦了擦眼淚，可見目前的狀態有多嚴重──發生大逃殺時，公會職員肩負的義務和重擔恐怕超乎想像吧。

有些迷宮裡的敵人會發動難以應付的「異常狀態」，因此無法貿然將探索者送進迷宮，所以大家只會拚命地探索安全且容易攻略的迷宮，這在某種程度上也是無可奈何的事。即使增強到一定的實力，恐怕還是有很多人離不開視野遼闊又方便戰鬥的『曙光原野』吧──要是我當時遲遲找不到伙伴也只有這個選擇了。

「我向您報告目前掌握的狀況，請到這個房間⋯⋯」

露易莎小姐打算帶領我們前往公會的會議室，就在這時──

「──魔物，有魔物跑到鎮上啦！快呼叫守備兵！」
「數量太多了，還在外面的探索者別逞強，快躲進建築物⋯⋯嗚啊啊！」

有某種非人生物在公會入口飛翔──它們對人類展開了攻擊；一位受到空襲卻束手無策的探索者受了傷，在逃進公會後不支倒地。露易莎小姐立刻跑到他身旁檢查傷勢，與此同時堅毅地大聲下達指令。

「馬上安排治癒師到場支援！請各位不要驚慌，守備兵馬上就會趕到！」
「沒辦法啊，露易莎前輩！守備兵都到鎮上戰鬥了，要呼叫他們支援很花時間！」
「怎麼會⋯⋯那、那該怎麼辦⋯⋯！」

為了讓心生動搖的露易莎小姐能夠冷靜，我將手放上她的肩膀。

「后、後部大人⋯⋯非常抱歉，身為公會職員的我明明不該亂了陣腳⋯⋯」
「我們能夠戰鬥，而且也對付過飛行魔物，請包在我們身上吧⋯⋯各位，準備好了嗎？」

我回頭發現所有人都已拿出武器備戰。雖然不清楚外頭的情況，但依照騷動的程度判斷，敵人恐怕遠超十幾二十只的規模。

「珠洲菜，不好意思突然向你提出要求，但請你立刻取得『堆鹽』技能。露易莎小姐，能向您要一些鹽嗎？只要有了這個，就能防止敵人將公會當作目標。」

現場也有許多等級只有１到２、連裝備都沒買齊的探索者。既然如此，我希望能讓公會成為安全的避風港，這樣才能無後顧之憂地戰鬥。

「鹽⋯⋯我明白了，我這就拿來。後部大人，請您守住這個小鎮⋯⋯」
「我一定會保護大家的。」

在八號區認識的人真的非常照顧我。我之所以能以探索者的身分活下來，都多虧露易莎小姐給了我傭兵券以及所有人的親切相待。

「有人先生，鹽已經準備好了！」
「很好！艾莉緹亞，你先對付襲來的敵人！我們接著會一口氣衝到外面！」
「好，我知道了。鏡花、特蕾吉亞，你們就在我身後吧！」
「瞭解！美咲就跟在我們身後，記得注意安全！」
「好、好的⋯⋯我會小心謹慎地跟上！」

打頭陣的艾莉緹亞衝出公會──魔物試圖從敞開的大門入侵，它全身都覆蓋著類似觸手的東西，模樣十分怪異。

（總之得先確認取得『支援攻擊２』後是否還能使用１⋯⋯發動『支援攻擊１』⋯⋯！）

「──喝啊啊啊！」
「喀唰啊啊啊啊！」

艾莉緹亞衝鋒的同時以拔刀斬的訣竅使出『開膛斬』。沒想到飛來的觸手底下竟然藏著一張血盆大口，它張嘴噴出黏稠的唾液並打算狠狠咬下，然而──

◆目前狀況◆

・『艾莉緹亞』發動『開膛斬』　→命中『飛空毀滅者』　支援傷害１１
・討伐『飛空毀滅者』１只

敵人被斬成兩截。被一分為二的魔物──「飛空毀滅者」以張嘴吞噬的氣勢撞進建築物，讓年輕的冒險者嚇得發出慘叫。我觀察著墜落地面的魔物，已經分開的上下顎長滿了銳利的尖牙，還沾滿綠色的鮮血──既然有這種魔物，難怪沒有人會想前往『午睡濕地』

我剛剛使用了『支援攻擊１』，但既然取得了『支援攻擊２』，我還是想試試它的效果。不過比起驗證效果，現在還是應該以掃蕩魔物為優先。

「聽好了，待在這裡的人都別出去！只要交給實力高強的探索者就能將傷害減到最低！」

等級９的艾莉緹亞的宣言聽起來相當有說服力，等級只有３的探索者都順從地接受了指示。

我們來到室外後，珠洲菜在公會入口的窗邊發動『堆鹽』──就在此時，鱗次櫛比的建築物上空又出現了飛空毀滅者，它們一發現我們便陸續俯衝而下。

我用黑檀彈弓狙擊了朝著自己衝來的魔物，但光是擊中一發還不足以抑制它的俯衝──！

「唔⋯⋯！」
「後部，危險啊！」
「──！」

五十嵐小姐用十字槍從旁擋住降落的飛空毀滅者，特蕾吉亞接著發動『暴風斬』將其擊飛。

「──珠洲菜、美咲！你們移動半步到我前方追擊！」
「好的，有人先生！──！」
「吃我一記骰子吧～！」

飛空毀滅者被擊飛到牆壁上後藉反彈之勢打算再度進攻，卻被珠洲菜的箭和美咲的骰子命中──攻擊力加上２２點支援傷害，讓我們無須藉助艾莉緹亞的力量也解決了它。

「竟然敢在鎮上肆無忌憚地亂飛⋯⋯有人，我們去幫助支援者和無法保護自己的探索者吧。」
「好，我明白了。這些魔物應該是在大逃殺時被放出迷宮⋯⋯也就是說，它們恐怕是從『午睡濕地』的入口傾巢而出。但那裡應該會有守備兵，我們就去其他地方巡視吧。」
「『『『瞭解！』』』」

飛行系的魔物正在八號區上空旁若無人地盤旋，還有一只色彩斑斕、體型大上一圈的魔物，摻雜在數十只飛空毀滅者中率領它們。

那大概就是『已命名』──我們必須設法打倒那傢伙。靠著珠洲菜堆鹽增加安全地帶的同時，還得拯救居民並將敵人殲滅。令人始料未及、波瀾四起的一天就這麼拉開序幕。

───

二　翼之狂宴

步出公會的第一場戰鬥結束後，艾莉緹亞告訴我當鎮上出現魔物時，只要讓探索證顯示地圖就能掌握魔物的位置。雖然在迷宮時要進入偵查範圍才能得知魔物的位置，但鎮上的魔物位置可以一覽無遺。然而，不知是否礙於隱私問題，地圖並未顯示非隊伍成員的所在地。

因為大逃殺而在鎮上出現的魔物約有五十只──其中半數跟隨在『已命名』身後。顯示在探索證上的『已命名』有如脈動般發出光芒，光是這樣就令人感受到不祥的氣息。

我在觀察地圖的同時發現了一件事──有幾隻魔物盤旋在法魯瑪小姐的寶箱舖上空。

（必須先從附近開始掃蕩魔物。設有傭兵介紹所的東方數量較多⋯⋯不知道傭兵會不會出外迎戰⋯⋯？）

除了我們以外，還有其他等級應該在３以上的探索者也來到了室外，不過也有人無法和魔物戰鬥，正面臨進退兩難的窘境。由於飛空毀滅者位於魔法和一般投射武器無法觸及的高度，因此無法將之擊落。

（要是敵人從空中發動進攻將難以迎擊，但也只能等待對方主動攻擊⋯⋯不，先等一下⋯⋯說不定能使用那個技能⋯⋯）

向位於空中的飛空毀滅者發動『後方立正』──即使這招確實可行，但只會變成自殺。我想到的是使用能讓箭矢確實命中的『百發百中』，這樣即使射程不夠，應該也能提供一定程度的輔助。

但每使用一次『堆鹽』就會消耗少量的魔力。我拜託珠洲菜定期灑鹽藉此擴大魔物無法接近的區域，所以不希望再讓她消耗更多魔力發動『百發百中』。既然如此──

「艾莉緹亞，能請你負責引誘敵人嗎！？」
「沒問題！雖然無法一口氣擊敗顏色不同的那只，但其他魔物不管來幾隻都能打倒！」
「很好⋯⋯五十嵐小姐，請您對艾莉緹亞使用『誘敵』！」
「知道了⋯⋯勇敢的戰士之魂啊，吸引凶暴魔物的鬥志吧⋯⋯『誘敵』！」

◆目前狀況◆

・『鏡花』發動『誘敵』　→對象：『艾莉緹亞』
・『飛空毀滅者』３只及『半鳥人』將攻擊目標變更為『艾莉緹亞』

（半鳥人⋯⋯原來還混進了這種魔物。不曉得它會發動怎樣的攻擊⋯⋯？）

「各位，還有新的敵人！『大家千萬要小心』！」

◆目前狀況◆

・『有人』發動『支援高揚１』　→６名隊伍成員士氣提升１１

我下意識地向大家喊完才發現──士氣解放後，理應得過二十四小時，『支援高揚』才能再度發揮效果，但剛才所有人的士氣都上升了。

（過了一晚⋯⋯不，士氣只要在宿舍休息後就會恢復嗎⋯⋯能不能設法在和『已命名』戰鬥前將士氣累積到１００呢⋯⋯？）

將士氣累積到１００就能發動強大的固有技能『士氣解放』，是否持有這張王牌將對戰局走勢產生莫大的影響。

等級３的隊伍就能擊倒飛空毀滅者，只要能捉住飛在空中的敵人便沒有那麼難應付；那只『已命名』應該也比我們至今迎戰過的強敵還要弱，但即使是小題大作，我也希望能夠發動五十嵐小姐的士氣解放技能『閃耀靈魂』賦予伙伴『戰靈』增加出招次數進行總攻擊。比起拉長戰鬥時間讓所有人都背負遭受攻擊的風險，這個做法相比之下要好得多。

『誘敵』對盤旋在寶箱舖上空的魔物起了作用，飛空毀滅者陸續俯衝到距離店家有些距離的大街。

「不管來幾隻都一樣⋯⋯太慢啦！」

艾莉緹亞以她的敏捷性輕鬆避開了觸手團的撞擊。在回避兩、三只魔物的攻擊後，五十嵐小姐和特蕾吉亞也從後方趕上，她們迎上在艾莉緹亞閃開後，以猛烈的力道撞上地板彈跳的其中一只魔物。

（這種時候還是該用『支援攻擊１』⋯⋯靠著固定傷害穩定取勝⋯⋯！）

「要上囉⋯⋯『雙重攻擊』！」
「⋯⋯！」

十字槍以快到眼睛跟不上的速度發出二連擊刺向飛空毀滅者；緊接著的是肉眼看不見的攻擊，最後則由珠洲菜的箭給予致命一擊。

『誘敵』的效果依然在持續作用，艾莉緹亞全身被帶有警戒作用的黃色光芒包覆，並確實地吸引了剩下的兩只魔物──攻擊被閃避的飛空毀滅者從地面反彈到牆上，並打算從上方大口咬下。但艾莉緹亞已經拔劍準備放出必殺一擊。

「──墜落吧！」

艾莉緹亞伴隨著比平常還要富有感情的吶喊，和進逼的飛空毀滅者錯身而過，與此同時連續揮劍出擊。

◆目前狀況◆

・『艾莉緹亞』發動『急升猛攻』
・第一段命中『飛空毀滅者Ａ』　支援傷害１１
・第二段命中『飛空毀滅者Ｂ』　支援傷害１１
・討伐『飛空毀滅者』２只

（能在騰空飛起的同時攻擊多只魔物，還真是恐怖的體術⋯⋯不，等等，還有半鳥人⋯⋯那傢伙究竟躲在哪裡⋯⋯）

──我突然打了個冷顫。每當出現這種感覺就沒有好事。

正如所料，不知道從哪裡傳來了歌聲──雖然無法辨識使用的語言，意識卻隨著歌聲為之動搖，像是差點就要被一口氣抽離。

◆目前狀況◆

・『半鳥人』發動『沉睡之歌』
・『艾莉緹亞』、『鏡花』、『珠洲菜』、『美咲』陷入睡眠狀態

「⋯⋯啊，後部⋯⋯」
「這首歌⋯⋯是⋯⋯」
「哥哥⋯⋯」
「⋯⋯各位，振作一點！在這種地方睡著會死啊！」

我並不是在開玩笑。如果無法掌握半鳥人的位置，連我們都會──要是它再發動一次『沉睡之歌』就會全軍覆沒，再也醒不過來。

（會讓我方陷入沉睡的危險魔物⋯⋯沒想到就是這傢伙⋯⋯該怎麼辦才好，再這樣下去就慘了⋯⋯！）

「──嗶咿咿咿咿！」

◆目前狀況◆

・『半鳥人』發動『呼朋引伴』
・２只『甜蜜鳥』發出呼應

──發出叫聲呼叫其他魔物──這樣下去會殃及睡著的伙伴。

（只靠特蕾吉亞和我有辦法戰鬥嗎⋯⋯就算支援特蕾吉亞，但要是被繞到後方就不妙了⋯⋯可惡，要是我的個人實力夠強，就不會演變成這樣⋯⋯）

遠處傳來了振翅聲──是鳥系魔物。兩只甜蜜鳥現身於空中，它們一發現我們的踪影，便像是在互相追逐似地盤旋於天際並開始歌唱。

◆目前狀況◆

・２只『甜蜜鳥』發動『翼之狂宴』
・有翼系魔物的攻擊力和防御力暫時提升

（沒想到敵方竟然會提升能力⋯⋯這些傢伙和至今遇過的魔物大不相同⋯⋯！）

將陷入沉睡的伙伴叫醒的方法──我打從心底渴望某項『支援』技能可以恢復異常狀態。我明明早就知道隊伍的弱點，卻遲遲無法找到補強方法，最終讓事情演變到這種地步。

陷入沉睡攸關死亡──我本來必須做好萬全準備，防止會讓大家無法行動的異常狀態發生才對。

「咕啊啊啊啊啊！」

兩只甜蜜鳥展開了攻擊──它們頭上被綠色羽毛包覆、艷紅似火的肉冠伸向前方，如同長矛般突出⋯⋯！

甜蜜鳥鎖定的對象並不是我們，而是『誘敵』還在作用的艾莉緹亞。

──若是想從這個距離保護她，我就只能藉由『後方立正』移動並挺身而出。但這樣我就──

（⋯⋯就算體力只有１，只要別死就好了⋯⋯比起讓艾莉緹亞死亡⋯⋯！）

特蕾吉亞使出『加速衝刺』，但仍然無法抵達艾莉緹亞的位置；我則是發動了『後方立正』試圖為艾莉緹亞擋下攻擊。

──我完成移動後，立刻看見兩只鳥振翅發出呼嘯風聲飛來的模樣。我祈禱體力值至少能剩下１也好，並準備保護艾莉緹亞，就在此時──

「──汪嗚！」

◆目前狀況◆

・『有人』發動『後方立正』　→對象：『艾莉緹亞』
・『席恩』發動『援護』　→對象：『有人』

（──是席恩！）

突然衝出的巨大身影以迅雷不及掩耳的速度擋在我和敵人之間。緊接著，獲得『支援防御１』效果的席恩受到甜蜜鳥的猛烈撞擊，不過──

「嗷嗚！」

◆目前狀況◆

・『席恩』發動『甩尾回擊』　→命中『甜蜜鳥Ａ』　支援傷害１１
・討伐『甜蜜鳥』１只

一擊必殺──現場還剩下一只甜蜜鳥，它雖然被席恩的防御加上『支援防御１』形成的保護牆彈開，但仍然拍著翅膀打算逃離。

就在這時，寶箱舖所在的街道又衝出一只巨大的身影。

「嗷嗚嗚嗚嗚嗚嗚！」

巨獸發出了簡直不像是狗的咆哮聲。席恩的母親撲向甜蜜鳥並用前爪將其一擊斃命。

（太強了⋯⋯看來這只母犬的等級相當高呢。）

「後部大人，您沒事吧！」

是法魯瑪小姐──對了，只要有兩只銀色獵犬就不用擔心她會陷入危機⋯⋯不，也不能這麼說。

（要是中了半鳥人的異常狀態，就連席恩它們也⋯⋯既然如此，只能靠我戰鬥了⋯⋯！）

「法魯瑪小姐，我的伙伴就麻煩您照顧了！我會解決最後一只的！」
「⋯⋯後部先生，您說的魔物在哪裡⋯⋯！？」

我無法回答──但魔物恐怕躲在屋頂的死角。要是想在一瞬間繞到那裡捉住它，我只能用那個技能賭賭看了⋯⋯！

（就算看不見，但只要進入戰鬥狀態就能發動⋯⋯拜託了，希望我的預測是正確的⋯⋯！）

◆目前狀況◆

・『有人』發動『後方立正』　→對象：『半鳥人』

我瞬間失去了知覺，下一秒──我已經站在手臂長出羽翼且擁有少女姿態的魔物後方。

「嗶咿⋯⋯！？」

它果然在屋頂上。我不顧立足處難以站穩、使盡全力抓住了半鳥人。

「──席恩！不，席恩的媽媽也行！接住我！」
「汪嗚！」
「嗶咿咿⋯⋯！！」

我並非擅長近身作戰的職業，但至少能阻止半鳥人拍動翅膀並一起墜落，而且──

席恩在我和半鳥人一起墜落時踩著建築物的窗框跳了上來，它用覆蓋著毛皮的背部完美接住我。

「唔喔喔⋯⋯！」

隨著衝擊降落地面後，我被從席恩背上拋飛──但我仍然緊抓著猛烈掙扎的半鳥人。

「哎呀哎呀⋯⋯明明這麼可愛，卻害大家傷透腦筋，真是不乖的魔物呢。」
「嗶⋯⋯嗶咿⋯⋯！」

半鳥人突然安分了下來──這也難怪，畢竟它正被法魯瑪小姐、席恩、母犬以及手持短劍的特蕾吉亞團團包圍。

此時代表所有人對半鳥人大喝的正是因席恩遭受攻擊而勃然大怒的母犬。

「──吼嗚嗚嗚嗚！」

它張開能將半鳥人一口吞下的血盆大口出聲威嚇，讓我也不禁感到害怕，半鳥人似乎真的以為自己會被吃掉，於是不再胡亂掙扎──或者該說被嚇到魂飛魄散了。

「總算是殺出重圍了⋯⋯特、特蕾吉亞⋯⋯？」

我一面提防半鳥人是否清醒一面撐起身體，特蕾吉亞此時從背後抱住了我。她的身體在顫抖──應該是我從屋頂跳下的舉動讓她擔心了吧。

「沒事，席恩有接住我。抱歉讓你感到這麼害怕，但除此之外我想不到其他方法了，畢竟光從背後抓住是無法擊敗它的。」
「⋯⋯⋯⋯」

特蕾吉亞一言不發地搖了搖頭，應該是我不需要道歉的意思吧。

「後部大人，鎮上有魔物就表示⋯⋯發生了大逃殺吧。」
「是的。法魯瑪小姐，這種名叫半鳥人的魔物會唱出讓人睡著的歌。要是沒有防止陷入睡眠的方法就危險了，請您躲進店裡吧。我會設法阻止敵人靠近的。」
「後部大人，店裡有幾樣裝飾品能防止魔物造成的『睡眠』，是找到重複的物品時收購的⋯⋯」
「⋯⋯真的嗎！？」
「原本應該要批發給專門販售的店家，但我認為您現在需要這些物品，請務必收下。我也會從附近的藥師那裡要來一些能從睡眠狀態甦醒的藥劑。阿修塔特，幫我把所有人送進店裡吧。」

為了和『已命名』魔物戰鬥必須先重整旗鼓。我和特蕾吉亞同心協力將伙伴和昏迷的半鳥人抬到名叫阿修塔特的母犬背上。由於半鳥人的外型和人類相近，所以很難狠心給予致命一擊──目前的情況並不適合說這麼天真的話，但我認為只要能防止它唱歌應該就不會造成威脅了。

這只體型嬌小的半鳥人差點害我們全軍覆沒──這場戰鬥也讓我們見識到異常狀態的恐怖之處。為了不讓大家受傷，我必須謹慎到過度才行。

特蕾吉亞仍然十分擔憂地跟在我身旁。這次也讓她操心了，我也希望今後能盡量避免這種豁出性命的特攻。

───

三　迎擊戰

將伙伴們搬進寶箱舖後，艾克和普拉姆從通往地下的樓梯跑了出來。他們應該是為了安全起見而躲起來的吧。

「媽媽，你回來啦！⋯⋯奇怪？大家都睡著了耶。」
「後部大哥哥，你沒事吧？這裡有點擦傷了，普拉姆幫你治療吧！」
「嗯，麻煩你囉，不過這點小傷沒什麼大不了的。」
「都是後部大人那麼亂來還一副若無其事的樣子⋯⋯原來您的膽子出乎意料地大呢。」

這表示我的外表看起來不像是會魯莽行事的人嗎？但正是因為有席恩它們，我才能使用那種方法，因此我不認為那是未經思考、有勇無謀的舉動。

「媽媽，可以把藥送給大哥哥嗎？」
「啊，不用了，藥水應該很貴重的⋯⋯」
「不，我這邊也有製作藥水的材料『療癒草』。將這種草乾燥磨成粉後可以治癒較淺的傷口，問題在於難以入口。」
「啊，原來如此。不過藥水本身就很貴重，材料應該也⋯⋯」

我正要向法魯瑪小姐詢問，她便伸出食指抵在唇上，大概是示意我別和她爭論吧。

「鎮上每個人家的藥盒都會有公會配給的幾種藥劑。雖然也有人拿去變賣，但我們並沒有使用⋯⋯這種時候至少允許我為您盡一份心力。」

法魯瑪小姐將療癒草的粉末放在紙上連同開水一起拿來。聽說藥粉很難入口，我已經做好了覺悟，但嘗起來就像是中藥，雖然味道辛辣，但並沒有那麼無法接受。

（喔⋯⋯體力恢復了。雖然傷口很淺，但能看見恢復的樣子就放心了。）

「啊哈哈，哥哥看起來很苦的樣子呢。」
「藥就是要苦才有效啊。」

艾克和普拉姆跑來撒嬌並看著我將藥服下。考慮到法魯瑪小姐和孩子的安全，我無法借走阿修塔特──但我希望能夠借助席恩的力量。

法魯瑪小姐和特蕾吉亞分頭讓大家服下甦醒藥丸，結束便回到了這裡。她看到我的臉就理解了我想拜託的事。

「席恩目前也還留在後部大人的隊伍，所以它才會將各位當成伙伴，並率先發現你們來到了附近喔。」
「啊⋯⋯不、不好意思，我忘記把席恩還給法魯瑪小姐了。」
「請別這麼說，畢竟之前也是我拜託您帶著席恩。倒不如說，這孩子想保護後部大人的行動讓我引以為傲。這孩子和阿修塔特一樣成長為富有勇氣的護衛犬了。」

法魯瑪小姐摸摸席恩的頭──坐著的席恩仍有成人的頭部高度，它在法魯瑪小姐作勢要撫摸便自動低下頭。

「⋯⋯請將席恩的力量借給我。我會輔助席恩提升防御力同時請它發動『援護』⋯⋯不過要拜託您讓這麼重要的狗狗擔任肉盾，真的很不好意思。」
「銀色獵犬經常在隊伍中擔當前衛，因為它們的體型比人類大而且體力充沛⋯⋯您不必客氣，無論您將席恩加進隊伍後希望賦予它什麼職責，都請儘管交付給它吧。」

要我來說的話，我認為法魯瑪小姐也擁有過人的膽識。雖然已經從探索的第一線引退，但仍會為了不使技能荒廢而進入迷宮並維持自身的等級，可見她的實戰經驗應該也很豐富。

「嗯⋯⋯這裡是⋯⋯」
「法魯瑪小姐的店⋯⋯不、不好意思，我們好像給各位添麻煩了。」

艾莉緹亞和五十嵐小姐醒了，美咲和珠洲菜也隨後睜開眼睛。

「啊⋯⋯有人哥哥，要小心那首奇怪的歌⋯⋯！」
「魔物呢⋯⋯有人先生打敗它了嗎？」
「只有我和特蕾吉亞幸運地沒有睡著。我們借助了席恩和狗媽媽的力量設法將魔物擊退了⋯⋯被抓起來的傢伙也在那裡。」

我向她們說明『沉睡之歌』的危險後，每個人都帶著緊張的表情望向昏迷的半鳥人。據說許多魔物只要被剝奪視覺就會安分，於是我們蒙住它的眼睛還塞住了嘴，並將它的羽翼和腳綁起來──如果不這麼做就會有危險，所以雖然看起來很可怜，但我們沒辦法再放鬆了。

「媽媽，這個有翅膀的女孩就是『魔物』嗎？」
「是呀，現在應該已經沒問題了，但還是不能太靠近喔。後部大人，既然您活捉了它，那就代示您打算讓它進入『魔物牧場』嗎？」
「『魔物牧場』⋯⋯？」
「就是訓練魔物並使其成為同伴的飼育場所喔。那個地方和倉庫差不多，養在裡面的魔物能用專用的『召喚石』呼叫。」

我至今都沒想過要活捉魔物，但如果正如艾莉緹亞所言，能讓魔物成為同伴──假如是擁有特殊能力的魔物就有利用價值了。

為了要瞭解『魔物牧場』的使用方式，就試著將半鳥人放進牧場吧。若是要將它肢解，視覺上多少還是會令人難以接受。

「話說回來，其他探索者在面對半鳥人時都會怎麼做呢？」
「半獸人等以雙腳步行的魔物多半都會襲擊與捕食人類，半鳥人雖然會蠱惑人類使其成為其他魔物的糧食，但本身並非肉食性，而是以草木果實為主食。因此探索者也不會將其視為眼中釘加以討伐，大部分都只取走少量的羽毛或指甲就會放它一條生路。」

對小孩子而言，對話內容到這裡就已經相當刺激了，但法魯瑪小姐又湊近我的耳朵說道：

「很多人會因為半鳥人的外型而忌諱與之交戰，但它的『睡眠』攻擊非常危險，所以率先鎖定它並擊敗也是標準的作法。」

面對礙於外型而難以打倒的魔物也能將其送進牧場。要是需要大量戰力時可以召喚魔物，那也能拓展戰術的範圍──今後和魔物戰鬥時就順便考慮是否要讓它們成為同伴吧。

「但也有許多魔物無法和人類溝通，所以只有一小部分能夠成為同伴。剛才在上空飛翔的觸手系魔物大概就不可能訓練，不過或許也有人擁有那種技能啦。」
「好的，我明白了⋯⋯法魯瑪小姐的說明讓我獲益良多。至於防止睡眠的裝備⋯⋯」
「就是這些裝飾品，有耳環、手鐲和墜子。所幸能防止『沉睡１』效果的物品經常會被發現，我才能為每個人各準備一種。」

『沉睡１』──也就是說還有更高階的睡眠攻擊。我必須理解今後並不能一勞永逸地抵擋睡眠攻擊，但現在只要有這些裝備就足夠了。

此外也能推測飛空毀滅者的『已命名』會造成異常狀態。想到這裡，我不禁為了保險起見先提出問題。

「不好意思，法魯瑪小姐，有沒有什麼物品能防止其他異常狀態呢？」
「我想想⋯⋯這個金屬護額擁有防止『混亂』的效果。您也需要這項裝備嗎？」
「為了安全起見，能和您借用嗎？因為不曉得敵人會出什麼招，所以我想先做個保險。」
「您設想得很周到呢。那就請您帶著它吧。」
「有人，由身為隊長的你裝備會比較好喔，你必須指揮大家，我希望你能隨時處於穩若磐石的狀態。」

我接受艾莉緹亞的建議戴上了護額。這條頭巾有著保護額頭的裝甲──我戴上後發現它有不會滑落的特殊設計，就防御來說也令人增添不少安心感。

「很好⋯⋯我們就一面留心半鳥人一面前往打倒『已命名』吧。」
「希望後部大人和各位伙伴武運昌隆⋯⋯您真的不帶阿修塔特一起去嗎？」
「之後也有可能發生緊急狀況，還是讓它待在這裡擔任看門狗吧。外頭還有其他探索者和傭兵，只要我們加入戰鬥，相信一定能夠擊退魔物。」
「大哥哥和所有人都要加油喔！」
「小心別受傷囉！」

我們接受了孩子的聲援再度前往室外──在空中盤旋的飛空毀滅者確認了我們的身影便陸續俯衝。

「──各位，我們要邊對付敵人邊前進！千萬不要勉強！」

我也不忘發動『支援高揚』提升大家的士氣。打頭陣的艾莉緹亞和席恩負責削減敵人數量，突破防線的魔物則由五十嵐小姐和特蕾吉亞進行反擊──接著再由我、珠洲菜和美咲展開間接攻擊。

「汪嗚嗚！」
「幹得很漂亮呢⋯⋯真是乖孩子。我們一起加油吧！」

艾莉緹亞向並肩作戰的席恩說道。從前方空降而至的飛空毀滅者幾乎已經不可能對我們構成任何威脅了。

敵人的總數在經過幾次交戰後大幅減少，原本會好幾只一起行動的飛空毀滅者消失無踪，只剩下『已命名』身邊的魔物群。

士氣隨著多次發動『支援高揚』也在逐漸累積。在我們抵達『曙光原野』前的廣場時──

「利瓦爾先生⋯⋯還有小圓！」

待在初學者聚集的迷宮『曙光原野』救助陷入困境的探索者藉此維生的利瓦爾先生──我也受過他的照顧，他正在和魔物戰鬥。平時總在這個廣場擺攤經營武器店的小圓則躲在建築物的陰暗處。

「大哥⋯⋯不可以過來！魔物、魔物的歌會⋯⋯！」
「有人，別從遮蔽物的陰暗處出來！只要進入魔物的視野範圍就會被攻擊⋯⋯唔喔喔！」

利瓦爾先生發出喊聲並舉起盾牌──宛如雷射光般的熱線頓時傾瀉而下。

◆遭遇魔物◆

★天降之死：等級５　戰鬥中　掉落物品：？？？

飛空毀滅者　１８只　掉落物品：？？？

半鳥人　２只　掉落物品：？？？
甜蜜鳥　３只　掉落物品：？？？

那是飛空毀滅者的『已命名』──從名字來看，它完全就是來取人性命的，不僅克服了飛空毀滅者無法進行遠距離攻擊的弱點，還擁有令人聞風喪膽的火力，再怎麼說也太犯規了。

「咕喔⋯⋯喔喔⋯⋯」
「利瓦爾先生，請您先退下！」
「知、知道了⋯⋯抱歉⋯⋯！」

利瓦爾先生拿著伙伴的鳶盾──他不惜借用別人的防具也要吸引敵人的攻擊。然而，熱線的威力非常驚人，盾牌無法完全抵擋住衝擊，他的體力也跟著被削弱。

受傷的亞人傭兵和探索者都倒在廣場上，也有人被熱線照射全身灼傷，所幸還有呼吸──但如果不盡早將魔物全數殲滅，只怕會有人因此喪命。

「──危險啊！」

我的心臟重重撞擊了一下。遠離燒得焦黑的攤子，躲在建築物陰暗處的小圓衝了出來。她是為了保護倒臥在地的探索者不被上空襲來的飛空毀滅者攻擊，才會挺身而出。

「席恩！」
「──汪嗚！」

席恩在我下令後對小圓發動了『援護』──『支援防護１』的效果能將飛空毀滅者的傷害減為零。

「就這樣躲在建築物的陰暗處！艾莉緹亞，你能吸引『那傢伙』的攻擊嗎！？」
「只要使用『音速強襲』就能躲開⋯⋯聽好了，有人！等我用『誘敵』吸引那傢伙，你就要在它飛下來時確實地給予致命一擊喔！」
「嗯，我明白了⋯⋯各位，小心別被熱線的流彈打中！我也不曉得自己的防御能不能完全擋住攻擊！」

『天降之死』在空中慢慢盤旋並鎖定下一個目標。但就如同艾莉緹亞所說，我可不打算在那傢伙降落後給它逃回空中的機會。

───

四　加護

席恩發動『甩尾回擊』，它揮動如同鞭子的巨大尾巴將飛空毀滅者擊飛，接著便和小圓一起藏身於建築物的陰影中──但戰況依然十分危急。

「利瓦爾，快逃！你再承受攻擊會死啊！」
「我才不會死呢⋯⋯該逃的是你們！那傢伙鎖定我了，我還能擋下一發！我隨後就會跟上，你們先走吧！」
「你又不是『盾騎士』，到底在說什麼傻話呀，利瓦爾大哥！」
「沒錯，我的職業是『伐木工』⋯⋯所以說啊，我要是不把那傢伙砍倒可是咽不下這口氣⋯⋯！」

我知道利瓦爾先生是認真的，但就我看來他沒有必要在此賭上自己的性命。

──但我能體會他的心情，甚至深有同感過頭了──利瓦爾先生並不是會任憑魔物宰割的人。

「──艾莉緹亞，你能避開那些熱線嗎！？」
「包在我身上！遇到困難只要交給我就行了⋯⋯鏡花，快對我用『誘敵』！」
「不⋯⋯！讓我跟你一起出擊吧！我可不能讓這麼大群的敵人圍攻你！」
「五十嵐小姐⋯⋯我也會負責援護！唯有那只『已命名』射出的熱線，請兩位要確實避開！」
「『瞭解！』」

她們衝出去後便傳來躲藏於廣場周圍建築物屋頂的半鳥人鳴叫聲，它們似乎早已摩拳擦掌地等著這一刻。

（要是它們用『沉睡之歌』，利瓦爾先生的隊伍就⋯⋯不對⋯⋯原來如此，『沉睡之歌』必須在近距離發動，否則就無法產生效果。要是它們能過來這裡⋯⋯！）

但半鳥人也沒那麼天真，它們並非毫無準備就在空中現身。三只甜蜜鳥呼應了剛才的鳴叫聲，它們先是透過『翼之狂宴』加強能力，接著其中兩只一起朝著這裡衝撞──另外一只則鎖定了舉著盾牌而使視野變窄的利瓦爾先生。艾莉緹亞察覺後立刻出手掩護。

──各地戰況起伏不定，我已經快要無法掌握一切並立即判斷了。就在我這麼想時，我突然發現視野一口氣拓展，甚至連空中的景色都盡收眼底。

◆目前狀況◆

・『有人』發動『鷹之眼』　→掌握狀況能力提升

『天降之死』位於上空，但我感覺自己像是在近距離觀察它。

它發出熱線後會有一段冷卻期。全身因高溫而發出白光的天降之死射出熱線，但覆蓋它球狀身體、顏色鮮艷的觸手仍然呈現紅得發燙的狀態──但距離再次發射只剩下十五秒，已經沒時間猶豫了。

考慮到體力減退時可能會出現的『凶暴化』，就必須在對方的體力下降到一半前發動猛攻並一口氣擊落。

「特蕾吉亞，你先把一只擋回去！另外一只交給我們！」
「──！」

特蕾吉亞使用『暴風斬』擊向其中一只甜蜜鳥並將其擊飛；另一只甜蜜鳥從旁掠過，打算將攻擊目標定為身為後衛的我們，此時箭矢、骰子和彈弓射出的金屬彈齊发，毫不留情地擊中它的頭部。所有人同心協力使用投射武器總算成功擊落了它。

特蕾吉亞引誘對方反擊，並在快要被擊中時發動『加速衝刺』進行回避。在敵人來不及減速撞上地面時，只有我發動了追擊──弓箭以及尚未習慣攻擊的美咲還沒做好投擲準備⋯⋯！

此時後方飛來了一樣武器──長長的鎖鏈前端綁著一顆鐵球。武器精準地將甜蜜鳥擊落，並為珠洲菜和美咲爭取到追擊的時間。

半鳥人突然從後方出現並開始唱歌──像是在表示即使沒有甜蜜鳥作為肉盾，但只要讓我們睡著就贏定了。

「──太天真了。我可是早就習慣熬夜啦！」
「──嗶咿！？」

◆目前狀況◆

・『半鳥人』發動『沉睡之歌』
・因『有人』隊伍所有成員的抗性而無效化

擁有少女外表的半鳥人似乎沒有想到自己的歌聲竟然無法發揮效果，為此露出驚愕的表情──但我們已經做好了攻擊準備。

「各位，上吧！」
「骰子攻擊～！」
「──喝啊！」
「──！」

我的彈弓和美咲的骰子擊中了一只半鳥人──半鳥人受到１１點附加傷害後搖搖晃晃地下墜；另一只半鳥人的羽翼則被珠洲菜的箭矢貫穿，頓時羽毛四散 ──雖然高度愈降愈低，但它仍然試圖再次振翅，特蕾吉亞突然躍起補上暴風斬。被擊飛的半鳥人雖然擠出最後一絲力氣想站穩腳步，但仍然因為體力耗盡緩緩墜落。

這麼一來就除掉棘手的敵人了，但還得捕捉昏迷的半鳥人。

我們回到了建築物的陰暗處並轉身回望。剛才用鎖鏈流星錘為我們提供援護的人就站在那裡。

「蕾拉小姐⋯⋯太好了，原來您沒事。」

使用流星錘的人正是傭兵介紹所的副所長蕾拉小姐。她渾身散發著強者的氣勢，一頭紅髮卻亂糟糟地和汗水一起黏在肌膚上，皮革制裝甲也有多處損壞。

「你沒事吧，有人。」

蕾拉小姐從後方跑來，我能透過斷裂的眼罩窺見底下的眼睛──眼睛雖然沒有受傷卻顯得黯淡無光，似乎無法視物。

「蕾拉小姐，您的眼睛⋯⋯」
「這個呀⋯⋯我以前曾和會剝奪五感的魔物戰鬥，視力由於受到攻擊而失明。但只要還留著一只眼睛，我就有辦法進行一定程度的戰鬥。」
「我明白了，我們一起作戰吧。請問您有回避系的技能嗎？要是沒有，現在就⋯⋯五十嵐小姐！」
「不要緊的，後部！如果是三次⋯⋯不，再四次的話⋯⋯！」

◆目前狀況◆

・『★天降之死』發動『閃光烈焰』
・『鏡花』發動『閃耀墊步』　→回避『閃光烈焰』

瞬間──包覆著『天降之死』的光芒集中為一點，火焰光束在下一秒射穿了五十嵐小姐原本所在的位置。五十嵐小姐做出分身躲開攻擊，卻沒有對發動追擊的飛空毀滅者還以顏色，而是選擇徹底回避。她既無法使用『雙重攻擊』，也無法使敵人反彈，卻很清楚無法一擊打倒對方時將會有什麼樣的風險。

飛空毀滅者的攻擊被五十嵐小姐躲過，珠洲菜在它提升高度前搶先射出箭矢，美咲也孤注一擲地扔出金屬骰子，造成了不少傷害──然而，缺少了前衛的攻擊無法將其一招斃命，因此遲遲無法減少敵人的數量。

「要是敵人能一口氣進攻就能將它們一網打盡⋯⋯真是煩死人了⋯⋯！」

艾莉緹亞一面這麼說一面算準時機將分批俯衝的飛空毀滅者擊落。『音速強襲』和『急升猛攻』都是一擊必殺的技能，但只要讓多數敵人同時進入『百花劍雨』的範圍就能更有效率地消滅它們。然而，敵人卻以兩、三只為單位錯開時間進攻──簡直就像是在戒備大範圍攻擊。

「小姐，我至少也能當箭靶⋯⋯」
「快逃就對了！要是你死了，你的伙伴要怎麼辦啊！」
「⋯⋯我明白了。之後就拜託你了⋯⋯唔喔喔喔！」

利瓦爾先生展現了他的志氣──他的伙伴位於隔著一座廣場的建築物遮蔽處，他背負著沉重的盾牌和伙伴會合的途中，還扛起了倒在路上的人。

「我們會吸引敵人的目標，快趁現在逃命吧！」
「──我還能擋下一發攻擊。一發的話⋯⋯喔喔喔喔喔！」

我請席恩負責保護小圓──如果讓它在這個情況下掩護利瓦爾先生，而我卻無法透過『支援防御』擋下熱線，席恩就會受到重傷。

我在腦中飛速地思考。目前還剩九只飛空毀滅者，『已命名』毫髮無傷──我此時發現了一件事。

（『天降之死』不再射出熱線了⋯⋯難道耗盡魔力了嗎？既然如此，它的目標究竟是什麼──）

──我彷佛看見理應不會有任何表情的彩色觸手團露出了獰笑。

剩下的飛空毀滅者襲向五十嵐小姐和艾莉緹亞，『天降之死』則鎖定了扛著倒下的魔法職探索者的利瓦爾先生。

◆目前狀況◆

・『★天降之死』發動『吞噬』

（──難道那傢伙⋯⋯打算對利瓦爾先生他們出手⋯⋯！）

利瓦爾先生正背著盾牌逃命，絲毫沒有察覺來自空中的威脅。怪物擁有能將成人吞下的血盆大口，而且正帶著殺意從天而降。

『天降之死』──這個名字並不只代表來自空中的熱線攻擊，而是如同字面上的涵義般代表那傢伙是『來自天空』並帶來死亡的存在。

戰鬥中的聲援以『支援高揚１』的形式發揮作用，士氣也提升到９９，只差一步就能施展士氣解放了。

我就算發動『後方立正』移動到利瓦爾先生身後，也只會害自己被吞噬。艾莉緹亞則要靠百花劍雨擊落五只飛空毀滅者，五十嵐小姐也要回避攻擊──沒有人能夠趕到利瓦爾先生身邊。

即使如此，我還是想拯救利瓦爾先生。我絕不能讓如此直率、即使明知沒有勝算也不逃跑的人就此喪命。

要是真的有方法可以保護利瓦爾先生，有什麼是我能做到的呢──我只能賭上唯一的可能性了。

那就是賦予我們加護的存在・阿里阿德涅。被稱為『秘神』的她，應該會有辦法──！

「──阿里阿德涅！守護『我接下來要支援的對象』吧！」

探索證回應了我的意志，它在我還沒有用手指滑動操作前就自動顯示了目標。

我除了隊伍內的成員外無法支援其他人──恐怕就連阿里阿德涅的加護也無法突破這項規則，但『對外支援』就能夠辦到。

我原本以為不會有機會支援其他隊伍，但這項技能為當下的困境帶來了一絲希望。

「──利瓦爾先生，我來『支援』您！」
「有人⋯⋯！？」

◆目前狀況◆

・『有人』發動『對外支援』
・『有人』發動『支援防御１』　→對象：『利瓦爾』

「嘰呀啊啊啊啊喔喔喔喔喔喔喔喔喔！」

眼看俯衝而下的『天降之死』就要將背著盾牌奔跑的利瓦爾先生一口吞下。

如此壓倒性的力量和速度，讓人一眼就能看出光靠『支援防御１』不可能完全擋下。天降之死正如隕石般從天上飛落。

祈禱終歸只是祈禱，果然還是無法上達天聽嗎？

我努力從喉嚨擠出聲音，但就算再努力大喊也無法傳到對方耳中了。

（我還是無法⋯⋯守護那個一生都如此為大家設想的人嗎⋯⋯！）

眼前的景象卻宛如要顛覆我的悔恨般產生了變化。

受到我支援的利瓦爾先生周圍的空間開始『扭曲』；我的腦中則響起了那位少女曾經否定自我存在的無機質嗓音──但她的聲音像是在鼓舞我般蘊藏了熱情。

『吾名為秘神阿里阿德涅，在此向信仰者及其盟友賜予加護！』

◆目前狀況◆

・『有人』向『阿里阿德涅』申請臨時支援　→對象：『利瓦爾』
・『阿里阿德涅』發動『護衛之臂』
・『★天降之死』的『吞噬』無效化

要是防護被攻破，利瓦爾先生就會連人帶盾被吞噬──但這樣的情形並未發生。

「那是⋯⋯什麼⋯⋯」

如果硬要形容，那就像只巨大的手。這只宛如巨人的手擋下了比利瓦爾先生還要大上一圈的『天降之死』

巨手放出滋滋作響的閃電，和緊咬不放的彩色怪物展開對峙。

機械巨人的手出現在原本空無一物的空間，保護了利瓦爾先生。

先是傳來了阿里阿德涅的聲音，接著出現巨人之手。這就代表利瓦爾先生被『秘神加護』拯救──也證明秘神果然擁有強大無比的力量。

「那、那到底是什麼玩意兒啊⋯⋯竟然從空中出現一隻手⋯⋯」
「利瓦爾大哥⋯⋯太好了⋯⋯你這笨蛋大哥可是差點就要被吃掉了啊！？」
「真是丟臉⋯⋯但那傢伙果然不是等閑之輩⋯⋯有人⋯⋯你究竟強到什麼地步⋯⋯」

一旦敵人從天而降，我就不打算放過它。我命令席恩掩護五十嵐小姐，讓她、特蕾吉亞和美咲有餘力發動士氣解放。

「就用這招解決它⋯⋯士氣解放，『閃耀靈魂』！」
「⋯⋯！」
「我要上囉～！士氣解放，『命運之輪』！」

◆目前狀況◆

・『鏡花』發動『閃耀靈魂』　→隊伍全體成員附加『戰靈』
・『特蕾吉亞』發動『三重扒竊』　→隊伍全體成員附加『三奪』效果
・『美咲』發動『命運之輪』　→下次行動將確定成功

「──如花散落吧！『百花劍雨』！」

巨人之手將『天降之死』彈飛──艾莉緹亞衝上前和戰靈一起發動斬擊之雨。

『命運之輪』的效果讓扒竊一次就成功，斬擊造成的傷害值和固定傷害值也不斷累積──加上戰靈總共２４段，傷害值合計大幅超越了３００點。『天降之死』的等級低於『鷲頭巨人兵』，體力被一口氣削弱，無法繼續飄浮在空中，最後墜落地面一動不動。

四處追擊五十嵐小姐的飛空毀滅者也在我、珠洲菜、席恩和蕾拉小姐的合作下被一一擊破。有了戰靈增加攻擊次數，掃蕩敵人變得毫無困難──我也瞭解席恩附加戰靈後將會成為相當可靠的戰力。

◆目前狀況◆

・討伐『★天降之死』１只
・討伐『飛空毀滅者』４只

魔物就此消失，戰靈則在我們調整呼吸時消散無踪──探索證的地圖上也看不到顯示魔物動向的指標了。

「成功⋯⋯了嗎？真的只靠你們就⋯⋯」
「是啊⋯⋯看來總算打倒魔物了，不過事情還沒結束喔。」

我們還得為所有傷者治療──也有些被熱線攻擊的人受了重傷。

我們先將勝利的喜悅放在一旁開始救助。守備兵此時終於趕來，他們陸續將傷者抬離現場。

我朝著利瓦爾先生等四人聚集的隊伍走去。利瓦爾先生看見我後便舉手微笑。

「我在不知不覺中完全變成接受幫助的一方了呢。你真是了不起啊。」
「利瓦爾先生也是⋯⋯您沒事真是太好了。」

利瓦爾先生苦笑著望向伙伴的臉。將盾牌借給他的大塊頭男子和女性弓箭手平安無事，男性魔法師卻失去了意識──原來利瓦爾先生救的是隊伍中的伙伴。

「我等等就會被這些傢伙訓斥啦，而且還給有人添了麻煩⋯⋯真是抱歉。也讓我向你道謝吧。」
「我們都是為了保護城鎮而奮戰，您不需要道謝。不過您剛才不能逃進迷宮嗎？」

利瓦爾先生的伙伴聽到我的問題後表示「一旦發生大逃殺，就得將所有出現在鎮上的魔物打倒才能再次進入迷宮」──雖然不清楚原因為何，但情況就是這樣。

如果搞不清楚狀況逃進迷宮就會產生誤算。我決定將這件事作為預備知識銘記在心，並準備加入救援的行列，就在此時──

「梅麗莎⋯⋯連萊卡頓先生也來啦。」
「有人先生，感謝您保護了城鎮。我們只能擊敗店舖附近的魔物，但無法靠近它們的老大⋯⋯您真的很強呢。」
「⋯⋯能讓我肢解『已命名』和其他魔物嗎？」
「嗯，麻煩你了。」

梅麗莎身上沾著的應該是飛空毀滅者噴濺的鮮血，她手上還拿著切肉刀──這表示他們父女也擁有能夠打倒飛空毀滅者的實力。

能夠確定認識的人都安然無恙固然是件好事，但城鎮受到了極為嚴重的損害，不僅到處都被燒得焦黑，廣場和建築物的牆壁也因為受到魔物的衝擊而變得坑坑洞洞。目睹這副景象，讓我們開始思考自己是否也能為了修復城鎮盡一分心力。

───

五　戰鬥之後

據蕾拉小姐所言，因為發生大逃殺而出現在鎮上的魔物會隨機襲擊居民並破壞建築物。

蕾拉小姐在得知大逃殺發生後便準備派出傭兵迎戰，卻在中途遭遇襲擊──倒楣的是，敵人當時正好從上空接近，它們一看到傭兵從介紹所出來就發動了空襲，整座建築物都受到波及。

亞人目前已被守備兵送往公會安置並接受治癒師的治療。據說被轟炸得全身焦黑的亞人正好擁有耐火的技能，因此才好不容易撿回一命。

「要是戰鬥再稍微拖久一點，恐怕會有人喪命了吧⋯⋯雖然稱不上平安無事，但這次之所以能化險為夷，都要歸功於有人你們。我也會作為見證者向公會報告你們的功績。」
「不，我⋯⋯」
「你不需要那麼謙虛。我目睹了你的勇敢以及隊伍成員活躍的戰鬥表現，這是無庸置疑的。」

蕾拉小姐因為我將特蕾吉亞納入伙伴一事對我抱持高度評價，如今她對我的觀感又更加提升了。

「但我從未見過你幫助利瓦爾時發動的技能⋯⋯有人，難道你擁有召喚的能力嗎？」
「召喚⋯⋯那招也許看似召喚。但請您將它當成防御系的技能吧。」
「唔⋯⋯這樣啊，看來那是你們隊伍的機密。但我以冒險者的身分待在別的區域時也看過類似的技能⋯⋯」

看來果然有其他『秘神』的信仰者──或者那真的是召喚魔法之類的技能呢？

但使用『召喚石』召換的會是活生生的魔物，不可能像剛才的『護衛之臂』一樣只召喚一隻手抵擋攻擊。

「蕾拉小姐，您說的⋯⋯是不是像召喚出巨人身體一部分的感覺呢？」
「不，當時召喚的是土製魔像，並將其作為盾牌。魔像能夠自由變形，所以應該也能將組成物變成手臂的形狀。」
「原來⋯⋯是這樣啊。謝謝您告訴我。」

果然不一樣──秘神是不會這麼簡單就被發現或目擊的。

即使在所有人面前請求『加護』，也無人能夠輕易察覺到阿里阿德涅的存在，連利瓦爾先生他們也以為『護衛之臂』是某人的技能。

（不過如此高超的防御性能真的是人類能夠施展的嗎⋯⋯不，我也希望自己的『支援防御』能夠到達這樣的領域啦⋯⋯）

伙伴委託了『運送員』搬運我們打倒的魔物。這次只要將一切都交給萊卡頓先生的肢解舖就行了。梅麗莎已經拖著店裡的平板車和萊卡頓先生一起將『天降之死』載走了。還是像這樣集中到一個地方比較省事。

「那我就去探望傭兵的狀況了，此外還得想想該怎麼重建被破壞的介紹所⋯⋯可能得暫時租借其他房子了。」
「如果有幫得上忙的地方，請儘管告訴我。我之前受到蕾拉小姐不少照顧，一直都希望能有機會報答您。」

蕾拉小姐沒有馬上回覆，而是緊盯著我。我原本對她的印象是位強壯的亞馬遜女戰士，但看見她邊將紅髮往上撩邊伸手觸摸耳朵的動作後，我稍微有所改觀了──沒想到這位揮舞著鎖鏈流星錘奮戰的女戰士也會害羞。

「⋯⋯和你一起作戰的伙伴都有體會到這種安全感嗎？感覺就像隨時被從身後包覆和守護⋯⋯」
「沒、沒有啦⋯⋯那個，我也不是很清楚⋯⋯」

由於蕾拉小姐在揮動流星錘時站到了前方的位置，因此我也對她發動了『對外支援』加以協助──原來『支援攻擊１』也會讓人產生這種感覺嗎？

「⋯⋯沒、沒事。剛才是我一時昏頭，你就忘了吧。這次真的要走啦。」
「好、好的⋯⋯您辛苦了，蕾拉小姐。」

蕾拉小姐一邊露出苦笑，一邊用手邊的綁帶將斷裂的眼罩修補好重新戴上，接著就這麼離開了。或許是因為展露了不像是自己的害羞模樣，讓她很難為情吧。

我環視廣場，發現小圓正和伙伴待在一起。她在我走近後，便從伙伴之中抽身來到了我前方。

「有人大哥，謝謝您救了我一命⋯⋯要是各位沒有趕來，我一定已經⋯⋯」

小圓臉色鐵青地顫抖。被一大群魔物襲擊會感到害怕也是難免的。

她販售初級武器的攤位雖然沒有受到直接攻擊，但熱線仍使得幾樣武器散落各處，或是慘遭破壞。

「你的武器損失慘重呢，送給初學者的武器恐怕會不夠吧。」
「是、是的⋯⋯我得確認一下損壞的武器種類，目前還很難說⋯⋯」

她從黑色箱子拿出大量『＋０』武器，那些武器作為初學者最先使用的裝備已經綽綽有餘。隨著職業種類不同，能夠裝備的武器也有所限制；如果不提升等級，有時甚至無法擁有強大的武器──不過我還沒有遇過無法裝備的武器。

但考慮到『大逃殺』可能再度發生，我就十分擔心小圓說不定又會被卷進戰鬥。

「雖然在這裡發送武器給初學者也是很重要的工作，不過⋯⋯小圓，你要不要成為我們隊伍的專屬商人呢？如果你已經和別人合作，我也不會強迫⋯⋯」
「⋯⋯這、這樣好嗎？我的等級還只有２⋯⋯」
「我的等級也還是２喔，小圓。今後就讓我們一起變強吧？」
「美咲小姐⋯⋯」
「說是這麼說，其實我只是想找個和自己等級相同的伙伴。因為珠洲菜比我高了一個等級，她已經開始大展身手啦。早知道我也學些劍道之類的～」
「我也是好不容易才趕上有人先生和大家的⋯⋯小圓要不要試著一起努力呢？只要和有人先生組隊，我想一定會很有趣的。」

我已經邀請過小圓，美咲和珠洲菜又更加賣力地游說。不知是否是因為放心下來，小圓現在才泛出淚水，她用五十嵐小姐遞來的手帕擦拭眼角。

「其實我也想過要提升自己的等級，也希望學習新技能。不過⋯⋯厲害的魔物實在是太多了，我連最初的迷宮都無法前進到最後一層，所以⋯⋯才會覺得自己真的很沒用⋯⋯」
「你不需要因為這樣就放棄呀。只要和別人組隊並互相幫助，無論什麼職業都能晉升更上層的區域。畢竟我光憑自己也是什麼都辦不到。」
「⋯⋯有人大哥比我還要強很多，也更加了不起。您就算面對那些魔物也絲毫不害怕，而且挺身而戰⋯⋯」
「後部確實冷靜到令人吃驚的程度，所以我認為跟著他準沒錯喔。而且雖然隊伍內都是女孩子，但後部也絕對不會對我們做出什麼壞事。」

（我反而懷疑大家是不是在晚上對我做了什麼惡作劇⋯⋯但要是我只是做了個煽情的夢，那就完全是害自己出醜，再說也很難確認夢境是真是假。）

不曉得以後能不能找到某種道具將睡著時的影像記錄下來呢？比如使用『紀錄石』之類的魔石──感覺迷宮國也不是不可能有這種東西。

「那、那個⋯⋯如果您不介意⋯⋯我想加入有人大哥的隊伍。」
「好啊，要請你多多指教了。我想委託你一些商人的工作，也會找機會讓你提升等級，畢竟有時候擁有技能會方便許多。」
「好的，那就麻煩您了！」

我剛來到迷宮國時，就見過這位綁著頭巾的商人少女・小圓，但一直到現在才邀請她加入隊伍──除了負責戰鬥的成員，我認為也不妨陸續增加在鎮上活動的伙伴。

「既然已經成功邀請小圓加入，可以讓我報告一下嗎？有人，這是使用『扒竊』取得的物品喔。」

艾莉緹亞將『天降之死』掉落的寶物交給我──那是一顆類似粉紅色魔石的東西。

我試著使用在法魯瑪小姐店裡打開黑色寶箱時拿到的初級鑑定卷軸──得知這是名為『混亂石』的寶物。

（魔石大多都會反映魔物擁有的特殊攻擊。這就代表『天降之死』的特殊攻擊是混亂效果⋯⋯不知道它是在使出這招之前就被解決，還是使用頻率本來就很低。無論如何，這次還真是幸運啊。）

混亂效果會使攻擊對象敵我不分，類似艾莉緹亞發動『狂戰士』時的狀態，但要是好幾個人同時陷入混亂，就會形成慘不忍睹的結果──屆時大家一定會自相殘殺吧。

將『混亂石』鑲嵌在裝備上似乎就能使用『混亂』系的技能。魔石能夠自由拆裝，也能同時裝備好幾顆，只要將混亂石和毒晶石一起裝上彈弓，我能附加的異常狀態就會增加為兩種。

我猜測半鳥人說不定也會擁有睡眠石之類的寶物──但半鳥人的額頭看起來沒有附著魔石狀的東西。不過經過訓練使它親近人類並能成功召喚後，屆時就可以將『沉睡之歌』加入戰術了。

這次特蕾吉亞和艾莉緹亞並未取得新技能，因此希望能在測驗時有效地利用新戰術。

「傷者都平安送走了，我們先回公會一趟吧。」
「說得也是，得讓露易莎小姐放心呢。」

我們回到公會後，正和其他公會職員忙著確認受災狀況的露易莎小姐一看到我們的身影，便立刻跑過來深深地低頭鞠躬。

其他職員和探索者也深深鞠躬──所有人都對我們離開這棟建築挺身與魔物戰鬥的行為表達感謝。

露易莎小姐過了一會兒才抬起頭，她的臉上不見平時親切可人的笑容，而是用嚴肅收斂的表情說道：

「後部大人的隊伍拯救了八號區的危機，請容我冒昧地代表公會以責任官的身分向您表達感謝之意⋯⋯真的非常謝謝各位。」
「我想今後的重建之路將會相當艱辛，但能安然度過這次的危機真的太好了。希望所有傷者也能早日回到工作崗位。」
「您說得是⋯⋯後部大人，謝謝您的鼓勵。」

一直處於緊繃狀態的露易莎小姐這時放心下來，她的眼裡立刻涌出淚水。我們成功卸下了她負責大逃殺事件時一肩扛起的重擔。不僅伙伴都放下了心中的大石，我也懷著相同的心情。

大家走向露易莎小姐為她加油打氣。站在我身旁的特蕾吉亞不知是否是因為戴著蜥蜴頭罩的緣故，她並沒有加入圍繞露易莎小姐的行列。

她的嘴角仍然沒有透露任何感情，一對蜥蜴眼睛卻直盯著我。她也鬆了一口氣──但隨即又想起我和半鳥人一起從屋頂躍下的一幕，因此也在微微發抖。

「我和大家都得變得更強，這樣才能更穩定地戰鬥。」
「⋯⋯⋯⋯」

特蕾吉亞點了點頭並用手抵著自己的胸口，像是在希望我今後能更加引出她的力量並加以活用。

───

六　飛躍級

八號區的受災狀況為建築物毀損約三十棟、傷者４６人，其中２人受到重傷。

大逃殺造成人員犧牲的例子並不稀奇，初級探索者也對『午睡濕地』敬而遠之，使該地區的魔物經常數量大增，因此會定期招聘高等探索者前往掃蕩。但這次大逃殺產生的速度大幅提前，才會演變成如此事態。

守備兵之所以不進入迷宮討伐魔物，是因為他們以維護治安為優先考量。只要出現業力值上升的反應，守備兵就必須火速趕到現場，但他們的人數不多，等級也不是那麼高。八號區的守備隊長等級為５，其他探索者幾乎都低於這個數字。

想在八號區提升等級相當困難──原本應該是這樣，但頻繁遭遇『已命名』魔物的我們，卻以超乎常理的速度在成長。

艾莉緹亞雖然還不至於連續升級，但探索證顯示經驗值的『氣泡』已經集滿了５個──只要再討伐兩只等級５的『已命名』就有可能升級。因為我們不眠不休地探索，所以沒有受到減少經驗值的懲罰，但之後也有必要確認停工休養會損失多少經驗值。

姑且不提這些，我今天也讓大家在外頭等待，獨自以隊長的身分向露易莎小姐報告戰果。

「請讓我看看您的探索證⋯⋯啊啊，果然⋯⋯」

◆本次探索成果◆

・鎮壓★１迷宮發生的大逃殺　１０００點
・『有人』等級提升至５　５０點
・『特蕾吉亞』等級提升至５　５０點
・『鏡花』等級提升至４　４０點
・『珠洲菜』等級提升至４　４０點
・『席恩』等級提升至４　４０點
・『美咲』等級提升至３　２０點
・討伐『飛空毀滅者』３２只　４８０點
・討伐『甜蜜鳥』５只　８０點
・捕獲『半鳥人』３只　２４０點
・討伐懸賞魔物『★天降之死』１只　１６００點
・隊伍成員信賴度提升　３００點
・總計救援３２人　９６０點
・救助『利瓦爾』　１００點
・救助『圓』１００點

探索者貢獻度　５１００

八號區歷代貢獻度排名　１

七號區貢獻度排名　３３２

露易莎小姐透過單片眼鏡確認數值，並發出感慨的嘆息。我們在鎮上穿梭並將視野所及的敵人全數打倒──相較之下，鎮壓大逃殺、擊潰『天降之死』和救援傷者獲得的貢獻度卻更多。

「露易莎小姐，您說的『果然』是指什麼呢？」
「後部大人在短期內做出十分巨大的貢獻，因此公會已經省去測驗並將各位編入七號區的排名了。恭喜各位一開始就能入住七號區的中級住宅。」
「中級住宅⋯⋯難道我們能租下一整棟房子嗎？」
「是的，不過是雙層公寓。也有許多人比較中意高級公寓的房型，所以會選擇集合住宅。」
「原來如此，畢竟生活習慣各有不同嘛。」

我們眼下即將入住的是租賃住宅，因此就算房子的條件再好，也無法長久定居──但總有一天能在某區找到會讓人想當作活動據點的地方吧。

「不過，不用測驗就直接通過感覺滿孤單的呢。在同一個地方下榻的隊伍『北極星』應該也即將接受測驗了，我原本是想和他們一起參加的。」
「北極星⋯⋯啊，他們幾位在今天清晨已經進入『呼喚之森』測驗了。」
「原來如此，難怪沒在鎮上看到他們的身影。測驗有時也會花上好幾天嗎？」
「沒錯，從七號區開始將要在迷宮內野營，所以這也算是一種訓練。魔物有時候會夜襲，應付這種情況也包含在測驗項目。」
「應該會是很有用的訓練呢。我會回去和伙伴商量，不曉得我們明天能不能接受測驗呢？」
「沒問題，如果您希望將日期訂在明天，我會為您辦理測驗手續。我接下來還得出席重建城鎮的會議，就先告辭了。」
「我想重建大概得花不少錢，如果公會的負擔太沉重，我們可以⋯⋯」
「不，為了因應這種情況，區域的居民設置了共同基金，原則上不需個人出資捐助，您有這份心意就好了。」
「這樣啊⋯⋯聽您這麼說，我就稍微放心了。傭兵介紹所也呈現半毀狀態，重建應該會相當費勁，其他建築物也在我們戰鬥時受到了損害。」
「即使如此，這次的受災情形也還算輕微。要是出現會在地面搞破壞的魔物，那就有可能造成極為嚴重的人員傷亡和財物損失⋯⋯為了不讓情況演變至此，我們才必須隨時將大逃殺的危險性保持在一定程度以下。」

我本來覺得露易莎小姐的態度愈來愈柔和了，她今天的應答卻給人生疏感，恐怕是因為她認為自己對大逃殺的事也負有責任吧。

「那些魔物對我們來說也相當棘手，就某種程度上來說，大家會對該區敬而遠之也是無可奈何。難道沒有方法能夠封鎖魔物的源頭嗎？」
「這個嘛⋯⋯魔物的產生是有條件的，如果能夠掌握原理，應該就有辦法抑制。但目前『魔物學者』僅公布了部分魔物的研究結果，其餘幾乎都尚未解明。」
「這樣就只能在危險度上升時進行處理了呢。希望您屆時也能通知我們一聲，我們已經擁有萬全的因應對策，應該能有效削減魔物的數量。」
「發生過一次大逃殺後，只要避免長期置之不理，就會相隔一段時間才可能再次發生。我打算重新訂定掃蕩魔物的行程表，藉此防患未然。」

其他地區恐怕也有類似『午睡濕地』這種容易被探索者回避的迷宮，大逃殺的危險就會在這些迷宮裡醞釀，使得公會救援者和守備兵疲於奔命。

最令人擔憂的則是某些迷宮的魔物連活躍於上層區域的冒險者都難以消滅，要是這些迷宮發生大逃殺──八號區都被破壞成這樣了，更遑論其他區域，只要稍有不慎就可能引發大災難。

探索者的使命便是勤奮探索合乎自身實力的迷宮，並與魔物戰鬥，以此防範各區的大逃殺。

然而，並非所有探索者都如此勤勞，且能夠持續賭上性命與魔物戰鬥。

「⋯⋯您是否覺得迷宮像是難以處理的燙手山芋呢？」
「老實說，我在某些部分確實這麼認為，迷宮既不好對付又充滿謎團，一不留意還會引發大逃殺。我有時也會想⋯⋯這個聚集如此多迷宮的國家是怎麼回事。正因如此，我才會想迅速提升名次，藉此理解各式各樣的事。」
「後部大人⋯⋯」
「啊⋯⋯沒、沒什麼，我好像有點耍帥過頭了。我完全不覺得以探索者的身分繼續旅程是件辛苦的事，不過也有很大一部分要歸功於伙伴的幫助。」
「不⋯⋯您剛才說得非常好，讓我深受啟發。身為後部大人的責任官，我今後也會盡全力協助您的，還請您多指教了。」
「也請您往後多多照顧。」

我們從座位起身握了手。當初面對這樣的美女還讓我慌張不已，想不到自己現在竟能如此沉穩──我來到迷宮國之後，應該也多少有些成長了吧。

───

七　商人工會

我報告完畢便前往公會附近的醫療所探視利瓦爾先生等人。擔任魔法職的男性似乎需要三天才能康復，但性命並無大礙。

「真是不好意思，有人應該也很累了，卻還讓你專程跑一趟。」
「戰鬥後確實多少會有些精神疲勞，但有伙伴陪在身邊，我的負擔倒也沒那麼大。」
「你真是了不起啊。開始探索到現在才短短四天，就已經這麼有模有樣。我想你待在這一區的時間大概也不多了，但我很高興能夠遇到像你這樣的新人。你以後也要偶爾回來八號區露個臉啊，否則我會很寂寞的。」

利瓦爾先生至今結識了許多新人，也目送他們離開。如果身為其中之一的我能讓他留下深刻的印象，那只有光榮兩字可以形容了。

「謝謝您，有人先生。都是托了您的福，我們才不至於失去伙伴。」
「利瓦爾大哥是我們的恩人，我們在初級迷宮受挫時都是多虧他出手相救⋯⋯」
「其實我應該要懷著野心繼續探索迷宮，並取得精良的武器和防具，而且奮发向上才對。所以有人對我而言是相當耀眼的存在。我們今後也會繼續做自己能力所及的事。有人，這麼說也許會為你帶來壓力，但我想將我們的夢想托付給你。」
「⋯⋯我明白了，我會盡力而為。但我認為您無論何時想再次追夢都沒問題喔。」

將救助初學者的工作傳承給後進並再次前往迷宮──每支隊伍都有如此選擇的自由。

「⋯⋯也對，你說得沒錯⋯⋯我在說什麼喪氣話啊。有人，你真的是⋯⋯」
「我也想試試被從未見過的強敵攻擊會是什麼感覺。雖然你可能會覺得我讓利瓦爾使用盾牌，還敢說這種話⋯⋯」
「我當時也非常害怕，完全拿那只顏色鮮艷到不行的怪物沒轍。但要是這麼乾脆地輸掉，我會難受到像要死掉⋯⋯所以就算再怎麼難看，我也會盡量掙扎。」
「我也是每次戰鬥都提心吊膽。不過因為有伙伴我才能忍住不逃跑。」

即使是一個人無法構到的高牆，也能透過互相合作或是靠著隊伍成員取得的新技能克服。

『後衛』在支援方面可說是無所不能，這也讓我感受到技能蘊含著無限可能性──但有些伙伴的技能也十分強大，好幾次都幫了大忙。

探索者眼中散發出的光彩──有朝一日我是否還能和取回野心的利瓦爾先生等人在其他地區重逢呢？只要生活在迷宮國一天，我都希望能夠如此期待。畢竟在無數隊伍中有緣相識的實在不多。

我和利瓦爾先生他們道別後，前往公會前廣場，法魯瑪小姐此時也帶著阿修塔特前來迎接席恩。

「啊⋯⋯您辛苦了，後部先生。身為鎮上居民的一分子，我由衷地感謝您出手相救。」
「真要說起來，我也是居民，要是鎮上不平靜我可是會很頭痛的。」

法魯瑪小姐優雅地微笑，望向正被五十嵐小姐撫摸頸部的席恩。

「透過今天發生的事，我清楚地理解這孩子的心情了。後部大人⋯⋯如果您願意，能請您今後也接納席恩作為隊伍的一分子，並帶它一起行動嗎？」
「呃⋯⋯這、這怎麼好意思，您真的要把這只毛髮蓬鬆、可愛又大只的狗兒交給我們嗎？」
「⋯⋯汪嗚。」

五十嵐小姐不知為何頗為慌張──大概是覺得要是席恩能成為伙伴就太高興了吧。

席恩相當可靠，我也多次受到『援護』的幫助，特蕾吉亞也不再害怕了。要是能讓它和艾莉緹亞一起擔任前衛，隊伍就能打下更穩固的基礎。

「謝謝您，法魯瑪小姐。我就承蒙您的好意，暫時照顧席恩⋯⋯不，應該要說，請問您願意讓它正式加入我的隊伍嗎？」
「好的，我很樂意。這孩子也相當開心⋯⋯哎呀哎呀，瞧它的尾巴搖成這樣。」

席恩維持坐姿擺動著大而蓬鬆的尾巴──和使出『甩尾回擊』時不同，它此刻的動作十分悠閑。

「阿修塔特，席恩會和後部大人一起變強回到這裡。你就放心地為它送別吧。」

阿修塔特先是靜靜地回望法魯瑪小姐，接著靠向席恩，開始為自己的孩子舔毛梳理。

結束理毛後，席恩將臉貼近媽媽的胸口，接著悄悄地離開了。阿修塔特眯細了眼睛來到我面前並趴在地面。

「它似乎是想請您多多關照呢。後部大人，您願意摸摸阿修塔特嗎？這樣就表示您知道它的意思了。」
「好⋯⋯我絕對會讓席恩平安回來。我想你一定會感到寂寞，但我們會定期來探望你的。」

我一撫摸阿修塔特的頭，它就閉上了眼睛──這個動作對它們而言象徵著服從與信賴。

「法魯瑪小姐，我想順便問一下，阿修塔特的等級是多少呢？」
「阿修塔特的等級是１３，它平時也會被公會派遣至上層區的高難度迷宮⋯⋯」

等級９的艾莉緹亞就已經強得不合常理了，沒想到阿修塔特比她還厲害──老實說，光靠它應該就能維持八號區的治安了，但強者或許也有自己的工作吧。

「不曉得席恩升到１３級是不是也會變得那麼大只⋯⋯」
「啊哈哈，要是等級和大小有關，艾莉小姐應該也會長得更高吧～」
「⋯⋯我、我才不矮呢，至少有達到平均身高啊。」

法魯瑪小姐聽到五十嵐小姐她們你一言我一語地拌嘴，再次露出笑容。她大概也有些寂寞，於是走到席恩身邊溫柔地撫摸它的脖子。

『曙光原野』前的廣場將進行整修，因此小圓收拾了平時擺設的攤位，將庫存放進我新租的倉庫，繼續以商人的身分活動。

小圓來到『歐雷爾斯夫人宅邸』後連連對豪華的內部裝潢發出讚嘆。我詢問她身為商人能夠辦到的事，她決定在談話室為我說明。所有人也決定一起出席，大家邊喝茶邊聽小圓講解，順便讓她自我介紹。

小圓解下了平時綁在頭上的頭巾，將有點扁塌的黑色鮑伯頭梳理整齊。平時頭上有戴裝備的成員都把裝備拆下來了，但她們也和小圓一樣困擾於頭髮容易變形這點。

「先前承蒙有人大哥介紹，請容我再次向各位打聲招呼。我叫做圓・筱乃木。」

大家也一一報上姓名；席恩並沒有進入宅邸，而是待在護衛犬專用犬舍──不過宅邸裡好像也有能夠讓狗進入的宿舍。

「我選擇的職業是『商人』並加入了『商人工會』，所以能透過探索證和八號區的工會加盟店取得聯繫。不僅在尋找特定商品時能進行搜尋，也能查詢能幫忙加工素材的工房位置，或者確認日程表。」
「這⋯⋯感覺就像是商人的特權呢。」
「這樣就不用一家家店找了，好方便喔。」
「可能的話，我想親眼見識盧恩石的裝備過程⋯⋯你能擬出加工的估價單嗎？」
「可以，不過必須花點時間才能向您回報。我也能提供多家同業的估價單。」

聽到小圓的這番話，讓人感覺像是只有『商人』才有網路購物的特權。隊伍中是否有這樣的人物，將會帶來巨大的差異。

「我也可以聯絡梅麗莎小姐，剛才討伐的魔物素材也能請他們加工喔。不過還需要隊長的確認，能請有人大哥按一下探索證頁面的按鈕嗎？」
「嗯⋯⋯這樣嗎？喔喔⋯⋯這還真是方便啊。」

◆有人・後部大人　來自肢解所的報告◆

・飛空毀滅者　３２只　無法作為素材使用
・肢解飛空毀滅者時發現『吸體石』２顆
・甜蜜鳥　５只　可加工為肉、武器和防具
・★天降之死　１只　表皮可加工為防具
・從★天降之死體內發現『紅色寳箱』

我原本就覺得整團都是觸手的飛空毀滅者大概沒辦法加工，結果也確實如此。既然無法食用，那就表示它跟外表一樣難吃吧。但由於打倒了很多只，所以出現了『吸體石』這項掉落物品──從名字判斷，效果會不會是能在攻擊時吸取對方的少許體力呢？

甜蜜鳥的羽毛好像可以製作為箭矢，所以就加工給珠洲菜使用；絨毛則能製作成帽子，我也決定委託店家進行加工。

至於天降之死──因為將觸手砍下後完整將皮剝下來了，似乎能夠將其做成防具。紅色寶箱的內容物比黑色寶箱少了許多，但寶箱可以說是探索者的浪漫，我仍然相當期待打開它的時刻。

「⋯⋯用『已命名』製作的防具是觸感如同橡膠的貼身裝甲啊⋯⋯」
「這、這種時候看著我也⋯⋯你希望我穿上嗎？我覺得裝備女用鎧甲就很夠了，而且艾莉小姐也有掛保證。」
「鏡花的鎧甲確實還能使用一陣子，但很少有機會能夠製作『已命名』的防具，要是有人能夠裝備就好了。特蕾吉亞怎麼想呢？」
「⋯⋯⋯⋯」

特蕾吉亞不清楚那是什麼樣的裝備，她只是不發一語地看著我。如果護身鎧甲升級為橡膠鎧甲將會如何呢──屆時她說不定就會失去頸部以下的蜥蜴模樣了。

「還是先看性能再決定吧。難得可以製作成防具，還是讓其中一個人穿上比較好。」
「那我就先向店家下訂囉。請問您要選哪家冶煉廠呢？」

只要請露易莎小姐介紹，她應該又會告訴我們一些內行人才知道的店家，總之我還是先試著瀏覽一遍店家名單。

「⋯⋯這家工匠等級最高嗎？等級７就跟法魯瑪小姐一樣了呢。」
「這麼說起來，支援者反而比身為探索者的我們還要強⋯⋯」
「工匠的等級也是從１排到７，我想並不是每個人都那麼強。但如果有心想磨練自己的技能，那等級就必然會提升吧。」
「那就決定委託這一家工匠⋯⋯嗚哇，預約的人還真多。原來如此，也會有這種情形啊。」
「得等上兩個禮拜呢，就算有人取消預約，還是得花不少時間⋯⋯大哥，您打算怎麼辦呢？」
「我知道了，還是動用人脈找找看吧。待會也問問女僕米蕾依小姐好了。」

目前還是下午，我們決定到晚餐之前都自由活動。我也先睡個午覺再收集冶煉廠的情報吧。

───

八　自我強化

我在午睡片刻後醒來，走出寢室就看見特蕾吉亞待在門外。

「你從剛才就一直待在這裡嗎？你也可以休息呀。」

特蕾吉亞搖了搖頭注視我──代表她想就這樣等到我起床嗎？

她的魔力也稍有減少，但五十嵐小姐因為使用技能耗盡了魔力，因此更加疲累。要是有可以立即恢復魔力的手段就好了，不過藥水目前很難取得，高品質的睡眠相較之下更為有效。

我來到起居室，便發現艾莉緹亞和其他人正在聊天。珠洲菜看到我便打算從座位起身打招呼，但我搶先制止了她。

「沒關係，你坐著就好。我要出門一下，需要幫你們買點什麼嗎？」
「要是有便利商店不知該有多好～不過這裡沒有那麼方便呢。」
「皇家套房已經備妥了生活必需品，而且好像每天都會補充。」

女性的必需品比男性更多──我之前都忘了這一點，但隨著房間的等級上升，生活用品的問題也得到了解決。

「如果東西不夠就說一聲，我會為各位準備的。」
「好，每個人都有一百枚金幣可以當作零用錢，你們就自由使用吧。就算這樣也還有超過一萬枚金幣，如果有大額開銷時，就所有人一起商量再決定是否動用吧。」

光是破壞神的賞金和素材就獲得了９５００枚金幣，加上『天降之死』的１６００枚賞金，就超過一萬了。

我希望避免有大額開銷時金幣不足的窘境，所以考慮將半數的金幣作為隊伍的共有財產，剩下的就平分給伙伴，但還是希望能省則省、以備不時之需。

「太厲害了⋯⋯我覺得八號區資金最多的應該就屬有人先生的隊伍了。」
「雖然在八號區是這樣，但七號區已經近在眼前。我們還有之前得到的寶箱，財產視內容物的價值而定應該還會稍微增加。」
「只要有小圓在，不僅買東西可以享有折扣，賣東西時似乎也能獲得更好的價格喔。在出售多餘物品時也有好處呢～」

變賣黑色寶箱中的大量初學者專用武器、防具也賺不到多少錢，所以影響不大，但在出售高價物品時，就算只提高一成的價格，金額也會大不相同。不過也要顧及和商人之間的交情，所以不應該每次交易都討價還價，而是只在重要時刻才進行交涉，我認為這點也相當重要。

「有人，你要不要幫大家看一下技能？我想你的技能應該也增加了。」
「啊，我們的之後再看就行了～要是大家同時拜託有人哥哥，他會很辛苦的。」
「我和美咲說好要在晚餐後再和他討論。」

要我一口氣檢視大家的技能也無妨，但我想審慎思考再決定應該取得的技能，確實應該要為每個人保留一定的時間。

話說回來，由於小圓加入了隊伍，所以我就要一個人睡了──隊上就只有我是男的，說起來也是很自然的事。

「那、那個⋯⋯鏡花大姊和有人大哥的感情很好吧。難道兩位是情侶⋯⋯不、不好意思，我現在問這種事還太早了呢。」
「不、不要緊的。那個⋯⋯我和五十嵐小姐原本是上司和下屬的關係，我們兩個一直都在一起工作喔。」
「原來是這樣呀⋯⋯就算是男女兩人，能透過工作增進感情也是件很棒的事呢。」
「唔唔，小圓的眼神太純潔了，好刺眼啊⋯⋯感覺快要被淨化了⋯⋯」

如果是美咲，應該會對我和五十嵐小姐的關係抱有各種遐想吧──不過實際情況就和我說明的一樣。

───

原本在房間休息的五十嵐小姐在我們聊著這個話題時來到了起居室。艾莉緹亞發現後便向她說道：

「啊⋯⋯鏡花，你怎麼了？要是身體不舒服要不要再休息一下？」
「我、我不是為了這個才起來的，只是聽到你們好像正在談論我的事。」
「我們聊到有人大哥和鏡花大姊的感情很好。我沒怎麼和男性接觸過，這樣的關係⋯⋯讓我覺得很羨慕。」
「⋯⋯咳、咳。後部，你是這麼跟大家說的嗎？與、與其說是感情好，不如說是身為團隊的⋯⋯事到如今，我就老實說了，後部對我而言是值得信賴的伙伴，我就是這麼看待他的⋯⋯」

要是我的回應是「原來您對我的看法是這樣啊」，周遭的人應該會看不下去。但當時的我和五十嵐小姐之間完全沒發生過任何會引人遐想的插曲──頂多只有她將冰咖啡錯買成熱咖啡，喝了一口後就整杯給我而已。

我想起五十嵐小姐命令我假日出勤，自己也幹勁十足地來到公司的模樣。很少有女性的身影能像她一樣和工作如此相襯。

「即使像這樣組成隊伍也能相處融洽，可見兩人果然從以前就相當合拍吧。」
「⋯⋯還、還好啦，就算現在才開始也不算晚就是了。要是個性不合，一起工作這件事本身就辦不到了嘛。」
「說得也是。哎呀，我還以為這樣說會惹兩位生氣。」
「啊～哥哥和鏡花姊姊的互動總是有種酸酸甜甜的感覺呢。偶像劇很常有只差一步就會發展成辦公室戀情，或者關係太親近反而習以為常的故事吧～」
「那、那種故事只會發生在偶像劇裡吧⋯⋯我才不會對上司有什麼非分之想呢。畢竟這樣對五十嵐小姐很過意不去⋯⋯」
「不過現在反倒是我成了部下。後部對我會抱持『過意不去』以外的想法嗎？」
「這、這個嘛⋯⋯真傷腦筋⋯⋯」

大家看到我搔著臉頰的樣子，都無言地笑了。我還以為五十嵐小姐只要被人開玩笑帶過就會生氣，所以對她端莊成熟的反應感到十分新鮮。

「⋯⋯呼啊。抱、抱歉，我才剛睡醒，感覺還有點累。」
「即使是在這麼高級的房間休息，但還是得睡上一晚才能恢復體力⋯⋯我好像也有點想睡了⋯⋯」
「艾莉小姐也要睡了嗎～？小珠要不要也睡個午覺呢？」
「嗯，我也用了一點魔力，所以⋯⋯呼啊⋯⋯」

美咲並沒有做什麼會消耗魔力的事，所以還很有精神，但珠洲菜使用『百發百中』和『堆鹽』消耗了不少魔力。

美咲表示想和珠洲菜一起休息，於是大家就留下五十嵐小姐和特蕾吉亞跑去睡午覺了。小圓好像也因為透過探索證向遠方下訂時動用了魔力，我在隊伍成員頁面確認魔力消耗的狀況時，發現她的魔力減少了四分之一。

「五十嵐小姐，我才剛睡醒，現在正在打發時間，所以請您慢慢休息吧。我等等會跟特蕾吉亞出去一趟。」
「是、是嗎⋯⋯出發之前要不要喝杯茶？話是這麼說，不過在這裡應該會由女僕小姐為我們端來。」
「有可能，但每次都麻煩人家也不太好意思，還是讓我去泡茶吧。」
「你坐著沒關係啦，畢竟隊長才是最辛苦的人啊。」

五十嵐小姐轉身走到坐在椅子上的我身後，並將手放在我的肩上。她的動作相當自然，稱呼我的那聲『你』聽起來也令人心曠神怡，輕易地讓單純的我沉浸在幸福的氛圍。

「那、那麼⋯⋯我就代替女僕⋯⋯」
「咦⋯⋯五、五十嵐小姐？」
「我、我的意思是，希望後部能讓我主動為自己的所作所為表示歉意⋯⋯我在公司時那麼為難你，這件事我還沒道歉夠呢。」
「不，我早就沒放在心上了⋯⋯」
「我不這麼做就無法安心，你要笑我也無所謂，但請讓我為你做點什麼吧。」
「我、我知道了。您說做點什麼是指泡茶這件事嗎？」

我也覺得這段對話就像是在隔靴搔癢。特蕾吉亞從剛才開始就一直看著這裡，五十嵐小姐似乎覺得愈來愈尷尬，她連耳根都紅透了。

──但我似乎有點會錯意了。在五十嵐小姐開口說出下一句話之前，我完全沒預料到這才是她感到害羞的主因。

「⋯⋯主、主人⋯⋯我這就去端茶，請您在這裡稍等一下⋯⋯！」
「⋯⋯啊，好、好的！我我我會在這裡等您！」

我慢了一拍才進入狀況，沒想到五十嵐小姐會提供這種根本不像是她會做的服務──雖說這是謝罪的環節之一，但沒想到她會以女僕的身分為我泡茶。她還傲嬌地說道「這種女僕可不是天天都遇得到的」，更加讓人無法招架。

「你好像很開心的樣子，太好了。今後有時間的話，你還要告訴我喜歡什麼喔。」

五十嵐小姐走向廚房，我則是茫然地望著她的背影──眼前這位魅力十足的女性是怎麼回事啊？她的反差之大讓我不禁懷疑，自己認識的人中是否真有這麼一號人物。

或許會有人認為她很做作，但個性單純的我仍會坦率地感到高興。不過我注意到特蕾吉亞正在盯著這裡，便順手調整了一下其實沒怎麼歪掉的領帶。

「特蕾吉亞，事情就是這樣，所以⋯⋯你可能有點累，但能不能陪我出去一趟呢？」
「⋯⋯⋯⋯」

特蕾吉亞點了點頭，卻好像有話想說。

「怎麼啦，特蕾吉亞⋯⋯對了，能先讓我選擇技能嗎？」

特蕾吉亞再次點頭──看來我的理解是正確的。由於等級提升，她能夠取得的技能應該也增加了，我也希望能趁空閑時先看看有哪些新技能。

我拿出探索證呼叫出特蕾吉亞的技能。身為前傭兵的她並未持有探索證，需要我代為操作才能顯示。

◆隸屬者技能詳細介紹『特蕾吉亞』◆

蜥蜴皮膚１　加速衝刺

探敵擴張１　警戒１　無聲步行

◆可習得技能◆

技能等級２

暗影幻步：可於回避的同時留下殘影，藉此迷惑敵人。
隱匿：直到發動攻擊為止將完全不被敵人察覺。必要技能：無聲步行
指尖術２：可解開中等難度的鎖或拆除陷阱。必要技能：指尖術１

技能等級１

雙重投擲：可同時丟出兩項投擲武器。
襲擊：在不被敵人發現的情況下發動攻擊，傷害將增加為２倍。
扒竊１：可在不被對方發現的情況下竊取指定的持有物。
破繩術：即使處於捆綁狀態亦可脫逃。
指尖術１：可解開低難度的鎖或拆除陷阱。
剩餘技能點數：７

「攻擊系和輔助技能都一口氣增加了⋯⋯喔，『雙重投擲』好像不錯呢。」
「⋯⋯⋯⋯」

『雙重投擲』和『襲擊』感覺相當實用，所以我決定優先取得；為了在發動『襲擊』後能夠安全逃脫，就必須學習『暗影幻步』──既然都選了這麼多，也希望擁有和『襲擊』息息相關的『隱匿』，但因為能夠取得『指尖術２』，連同『指尖術１』一起學習也不失為一個方法。

「特蕾吉亞，我能將開鎖和拆除陷阱的職責交給你嗎？」
「⋯⋯⋯⋯」

特蕾吉亞似乎對戰鬥技能很感興趣，但我說明已經預計取得三種技能後她就點頭同意了。『隱匿』則等升到下個等級後再納入考慮。

「為了發動『雙重投擲』就必須購買投擲武器。我想將短劍留著用於近身作戰，那就得找個不會太重的武器，這樣才方便和短劍同時攜帶。」
「⋯⋯⋯⋯」

投擲匕首只要全部丟完就沒有了，美咲的鐵骰也幾乎都是有去無回，只能接受投擲武器的設定了。如果有武器能夠自帶重回手中的效果，那麼只要利用魔石或盧恩石加以強化，特蕾吉亞的瞬間攻擊力應該就會大幅提升。

「好啦，接下來看看我⋯⋯」

我在探索證上呼叫出自己的技能。看了特蕾吉亞的技能卻不讓她看自己的也有失公平，我便決定讓她在一旁觀看。

我睜大了眼睛──因為上面顯示了好幾種我一直渴望擁有的技能。

◆已習得技能◆

支援防御１　支援攻擊１　支援攻擊２　支援恢復１　支援高揚１

鷹之眼　對外支援　後方立正

◆可習得技能◆

技能等級２

支援防御２：可替前方伙伴製造出與自己防御能力同等的保護牆。相同屬性技能不可重複。
殿軍之將：位於前方的伙伴愈多，將更加提升自己的能力。

技能等級１

支援協力１：可使前方伙伴發動聯合技。
支援魔法１：消耗５０％魔力和威力加強前方伙伴的魔法。
支援回避１：前方伙伴將有低機率發動『絕對回避』

支援召集１：召集附近隊伍進行後方支援。
協助補給１：可將少許魔力分配給前方伙伴。
正後方：消耗５點魔力，定時間內可使視野範圍擴展至後方。
後方復燃：後方遭受攻擊時自動進行反擊。
剩餘技能點數：３

「⋯⋯⋯⋯」

特蕾吉亞默默指著『殿軍之將』這項技能──殿軍指的是撤退時待在隊伍後方，應付敵方攻擊同時逃跑的敢死隊。但作為『後衛』的技能則能克服無法對自己進行支援的弱點。

理所當然，只要我的防御力在逃跑時有所提升，那就能遵循「殿軍之將」的字義盡到保護大家的職責──不過也要看防御力能提升多少啦。

特蕾吉亞的手一直指著同一個地方，似乎是在建議我取得能保護自己的技能。

「技能點數真的不夠用了呢⋯⋯為了保險起見就取得『協助補給』吧。至於『殿軍之將』⋯⋯」

比起這招，我還有很多想學習的技能，像是『支援協力１』──只要運用得宜就能提升隊伍全體的攻擊力。

「⋯⋯⋯⋯」
「你、你這麼希望我取得那項技能嗎⋯⋯？」

特蕾吉亞點點頭。見到我遲遲沒有做出選擇，她又點頭示意──要是我選擇了其他技能，無法言語的特蕾吉亞將會多麼沮喪呢？想到這裡我就無法對她的意見視若無睹。

「⋯⋯好，我明白了。雖然以自己為優先並不符合『後衛』的本分，但我確實打算在日後取得喔。」

特蕾吉亞這才一言不發地縮回手指並將兩手放於膝蓋。蜥蜴頭罩則漸漸羞紅。她大概是對技能提出意見後，才開始感到不好意思吧。

「老實說，我也想至少取得一項可以強化自己的技能，不過要是少了伙伴，再怎麼強化也沒有意義。有特蕾吉亞和大家待在前方，我就能變強了。」
「還有那種技能呀⋯⋯？後部的職業真的是愈在後方就愈強呢。」

聽到我這麼說的五十嵐小姐將茶杯放在我面前。特蕾吉亞怕燙，所以五十嵐小姐將準備好的茶倒進玻璃杯拿來了。

「對了，五十嵐小姐，我想試試剛才取得的技能，能請您背對我嗎？」
「好、好的⋯⋯這樣可以嗎？」

（將我的魔力分給五十嵐小姐⋯⋯『協助補給』！）

「⋯⋯！」

我的身體放出的魔力和五十嵐小姐的魔力互相結合後，產生更多的力量──我的魔力減少了整體的十分之一，五十嵐小姐則恢復了大約兩倍的魔力。我們兩人的等級有落差，魔力值應該也不同，但魔力的恢復量看來比我消耗的還要多。

「啊⋯⋯我剛才還覺得身體有點沉甸甸的，不過疲勞好像減輕了。這也是後部的技能嗎？」
「是的。我現在能使用自己的魔力恢復前衛的魔力了。這麼一來，今後要是您的魔力不足以施展『閃耀墊步』，我就能為您補充了。」
「⋯⋯你、你連這點都想到了，所以才為我取得這項技能嗎？因為你放心不下我⋯⋯？」
「畢竟回避很重要，而且您也恢復了精神，真是太好了。」

五十嵐小姐好像想說些什麼，卻不知道該如何開口，她於是在我們對面坐下。

「⋯⋯後部就是這樣，特蕾吉亞也要加油啊，他就是不肯給人報恩的機會。」
「⋯⋯⋯⋯」
「呃⋯⋯特蕾吉亞也在思考這種事嗎？我總覺得你跟五十嵐小姐反而更能心靈相通耶⋯⋯？」

特蕾吉亞只是回望有所動搖的我，並和五十嵐小姐彼此點了點頭。有些事果然只有同為女性才能互相理解啊──雖然有點不甘心，但看著兩人之間和睦的氣氛，感覺倒也不差。