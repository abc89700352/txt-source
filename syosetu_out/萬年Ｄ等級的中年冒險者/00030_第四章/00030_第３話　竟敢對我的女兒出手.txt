聖德古拉王国・王宮──謁見之間。

一臉嚴肅喃喃自語的初老男性，坐鎮於最深處華麗的王座上。

「嗯姆⋯⋯難道是真的嗎？沒想到那把劍是真的，而拔劍者出現了⋯⋯」

雖說頭上開始混雜白髮，只是看起來面容精悍如若壯年。
並且擁有一身強健的筋肉似乎至今也還沒衰弱。

他正是這個国家的領袖，聖德古拉王国国王──費爾納澤＝萊亞＝聖德古拉。

就是俗稱費爾納澤三世的那個人。

「只不過豬頭帝被討伐的時候，有很多騎士和冒険者目擊到了。伊莎貝拉卿看來不像胡說的樣子啊。」

提出見解的是作為費爾納澤右腕之稱的大臣。

───

就在幾個小時前。

雖然前幾天收到了報告，但是伊莎貝拉子爵親自來到謁見之間，詳細報告了事情的經過。
雖然是女人，卻給人留下了武人的風度、總是泰然自若的印象⋯⋯只不過變得滿臉通紅，興高采烈地述說，讓王和大臣都非常困惑。

那宛如是戀愛中的少女一般。

「不管怎樣，他確實討伐了特Ａ級危險度的豬頭帝。必須給一些褒獎。然後以此為由傳喚那人，然後親眼確認他的真假。」
「原來如此。那麼，我馬上給您安排吧！」

對王的命令大臣點了點頭。
畢竟聽說那人雖是一介平民卻是騎士學院的學生，他估計傳喚起來並不難。

如果伊莎貝拉所說的內容是真的，那麼我們一定要在政治上有效活用其存在。
幸虧如果是騎士學院的學生，至少不會與這邊敵對吧。

「父親大人」

這時，在王座之間，來了一位美到讓人眼前一亮的少女。

這個地方能夠沒有任何預約就進入的人是有限的。

但她是其中之一。

那是理所當然的，她的名字是菲奧拉＝萊亞＝聖德古拉。
被譽為擁有該国第一美貌之人，国王費爾納澤的女兒。

「哦，菲奧拉。怎麼了？」

費爾納澤直到剛才為止一直繃緊面孔，像假的一樣馬上鬆弛。

他溺愛女兒這方面非常出名。

「⋯⋯非常抱歉，父親大人。其實，剛才的話我偷偷聽到了⋯⋯那個，真的要叫他來王宮嗎⋯⋯？」

面對女兒戰戰兢兢地詢問，費爾納澤有點意外歪了歪頭。

「這麼說來，跟你在同一個學院上學。難道你認識嗎？」
「嗯，嗯⋯⋯」

菲奧拉咬了咬牙，不快地點頭，

「其實，那個⋯⋯我啊，被那位大人表白過幾次了⋯⋯」

當然是假的。

兩眼水汪汪，眼看快要哭出來的也是演技。

「你說什麼？」

但是在對女兒充滿溺愛的費爾納澤王眼裡，是不可能會留意到這樣的事。
原本一臉的慈祥，眉毛突然豎起。

「是怎麼一回事啊！以平民的身份，竟然要對我的女兒出手⋯⋯！」
「當、當然，我堅決拒絶過了。但是啊，他好像完全聽不進⋯⋯」

面對聲音顫抖、低著頭的菲奧拉，費爾納澤更粗聲粗氣地說道。

「哦，多麼可憐的女兒啊！想必是遭遇可怕的事情了吧！」

費爾納澤腦海擅自浮現出，那人語言不通，並對女兒強行糾纏的情景。
雖說他被譽為賢帝，但一提到女兒，就無法冷靜地作出判斷了。

另一方面，菲奧拉確信進展順利⋯⋯並在內心偷笑。

但是即便如此為了不出任何意外，繼續輕聲但明確地斷言道。

「⋯⋯父親大人。我真不敢相信他就是神劍的使者啊！」
「或許是吧！」
「另外假如這是假冒神之名行騙，那更不可原諒啊⋯⋯」

然後她提出了建議。

「⋯⋯因此我覺得，我們應該拜託能接受神諭的聖女艾莉斯大人進行審問。」

───

目送著菲奧拉從謁見之間離開。

大臣心中嘆息，＜不好⋯⋯這下可麻煩了＞。

實際上神劍是真是假都無所謂。
即使作為傳說之劍，它也有與之相稱的性能。

只是向国內、外廣泛地發表時，也打算避免以＜神之劍＞的冠名。

為什麼這麼說呢，畢竟冠以神之名以後，會引起各種各樣的麻煩。
在這個世界上，民眾普遍以三大女神為中心的眾神進行信仰，但其解釋卻各式各樣。

到底哪種是正確的，這只有神才知道，要確認也並不容易。
如果拙劣地把神劍之類對外宣布，就很可能會產生多餘的摩擦。

但是現在，已經不可能低調行事了。
王接受了愛女的建議，委託聖女艾莉斯進行審問。

她不僅是這個国家，而且在周邊国家都具有強大影響力的大神殿最高指導者。
如果被她認定為異端的話，那把劍恐怕會被破壊吧。
也不知道那個使用者會被如何對待。

「但願是真的，就好了⋯⋯」

雖然嘴上這麼說，但大臣還是覺得希望不大。
畢竟那只是象徵愛與勝利之女神，維妮婭製作出來的神劍而已⋯⋯不湊巧聖女艾莉斯所宣布的教義裡，不推薦男女有情慾關係。

造成神殿的聖職者們都被要求單身。

對於一般信徒，結婚和生育是自由的，但是也禁止婚前性行為和娶複數人為妻。
對於她這樣的教義，基本一夫多妻的王家和其它貴族也經常感到困擾⋯⋯那姑且不論。

據伊莎貝拉所說，那把劍無論能力還是性質都正好與聖女的教義完全對立。

───

另一方面，在謁見之間離開的菲奧拉。
表情一下變得惡役，滿意地笑起來。

「呵呵呵，進展非常順利啊。這下那個男人也就完蛋了。那個肯定是假的。要說原因的話，那種男人被神劍選中，是不可能的⋯⋯」
「⋯⋯殿下」

瑪莉沙在房外待命，對主人的圖謀深深地嘆息著。