這種寒冷，有什麼辦法嗎？我想在凍結之前結束談話。

「那麼，我到底要做什麼，才能活下去呢？」

「我們尊重同伴。於是我提議了，你讓參加【威士・奧爾】。」
「好。」
「這麼快。」

面對夏洛特的驚訝之聲，提亞利埃塔不由得露出了笑容。

「原來如此，【威士・奧爾】不是個人，而是代表研究者集團的吧。嗯，確實很有魅力。說實話，我現在就想把工作扔下，轉移到你們的據點了。」

「話說得快，幫了我大忙，可還沒告訴你。如果您不參加學院活動的話，我會很為難的。」

「嗯。麗莎君想要的【情報】，和什麼有關係嗎？」

「好的。只是在問這之前，正式參加是有條件的。」

「當然了。保密義務當然不用說，目前不探究你們的真面目，這樣嗎？」

「請讓我先確認一下，妳是否值得信賴的人。」

「很遺憾，我不是值得【信賴】的人。我知道這一點。只是妳可以【信任】我。」

麗莎補充一句給困惑的夏洛特。

「基於人性，相信她是很危險。但是如果利害一致的話，她就不會背叛。」

「沒錯。即使揭露你們擁有魔族的消息，對我也沒有什麼好處。不如說只有壞處。」

「這麼說來？」

「魔族是擁有特殊屬性的人，所以有只有他們能使用的魔法。不管麗莎君是不是這樣，絕對應該避免被捕的事。如果被殺了就麻煩了。魔族也許不能和人相容，但是【應該生存】是我一貫的主張。」

「路塞亞納爾教授是個好人。」

「……認識上的分歧會產生以後的爭執，所以趁早糾正吧。我是個無情的人。為了研究他們特有的魔法，認為應該發揮魔族。因為和古代魔法的關係很強呢。」

在魔族社會中【留下記錄】的意識很薄弱。多數族群依賴同族內部的口傳傾向很強。在個人主義色彩濃厚的背景下，強者越是長命。
因此，在與他們敵對的人族方面，魔族特有的魔法研究極為困難。

「嗯……對不起，我不太明白。從對被虐待的魔族們友好的角度來看，果然是個好人吧？」

提亞利埃塔覺得，這個白面具的想法太幼稚了。

「我不認為我和他人友好。如果必要的話，表面上會修飾，但是不擅長。就生死而言，即使找到了某人的屍體，無論是親兄弟還是陌生人，都只能產生【啊，好可惜啊】的感想。」

「太可惜了，是嗎？」

「從【死】中得到的情報之類很高。無論是人還是魔族，活著是為了什麼才有價值的。死了沒法研究了。」

「只把魔族當作研究對象？」

「稍微有點不同吧。世界上所有能生存的東西都是我的研究對象。包括我自己哦。」

「果然還是不太明白……」

夏洛特歪著小腦袋。
麗莎又補充了。

「她的想法和魔族的思想很接近。我們把自己的利益放在第一位。所以如果有必要的話，不會猶豫奪走他人的生命。」

「我並不認為殺害是正確的，不過大概是吧。並且『自己的生存』是最優先事項。所以‘為了誰而獻出自己的身體’絕對不會發生。」

「我們有時也把同胞和主放在最上位。現在的我是那樣。我跟你不相容。」

「並不是不能理解。我只是不想那樣做而已。」

輕飄飄地接受冷徹的目光的提亞利埃塔。
夏洛特望著兩人的對話，仍然很頭疼。

「我覺得你不能理解也沒關係。現在是罕見的純真和清廉。因為那個無垢的心是研究者必要的要素。雖然和我是相反的極端，嗯，這樣就可以了。」

正因為是純粹，好奇心沒有界限。
只要交談就認定是未滿15歲的少女。如果已經超越自己的天才就這樣成長，將會成長為震撼世界的人物吧。

（真糟糕。作為教育家的我，雖然很討厭，但是還是想守護她的成長。）

在此基礎上，我強烈地希望能夠成為共同研究的立場。

「那麼，雖然我是只考慮自己的理性思考者，妳覺得這樣合適嗎？」

夏洛特把視線轉移到了麗莎。

「利害沒有逆轉的時候，我覺得【信任】她是可以的，麗莎是怎麼想的？」

「……做出奇怪的動作馬上處理。如果是加上那個條件就可以了。」

「嗯……」

「哈哈，你真可愛。麗莎君也不應該為難同伴吧？」

「熟識的話。」

「嘛，利害是不會逆轉的。那一點可以放心。不管怎麼說……」

提亞利埃塔在寒冷中顫抖著笑了。

「你們和《濕婆》有關係嗎？」

「知道了嗎？有說過什麼提示嗎？」

啊，麗莎抱著頭。
提亞利埃塔也擔心這孩子是不是太坦率了。

「雖然還有一半疑問，但是，嘛，在確證之前，就是那個通信魔法。」

提阿利埃塔雖然看過【威士・奧爾】的所有論文，但是沒有任何關於通信魔法的。
本來大規模的魔法術式必要的魔法，簡略化就能實現。但是為什麼完全沒有提到？

也許【威士・奧爾】中，沒有能使用這種魔法的人。

「據我所知，能做出如此破天荒的人只有一個人。可是他很聰明。好像不屬於威士・奧爾。」

如果變成，濕婆是向她們提供技術的立場，不過，不是『朋友』的那樣。
因此考慮到【有聯繫】的程度。

「怎麼樣？」

那個慧眼，真是不好意思

「雖然被表揚了並不覺得噁心，但是這樣的話就會浮出疑問。他也是這樣，你們真正的目的是什麼」

夏洛特等了一會兒說。

「我們都是守護濕婆的人，讓世人知道它的偉大並在背後守護的會。簡稱為【濕婆守護會】——觀察者。又名【圓桌會議】。」
（譯註：觀察者，ベオバハター。）

該從哪裡開始吐槽？

「啊，圓桌會議是活動的主體。那麼，會議的正式名稱應該是【卡美洛】吧？
（譯註：卡美洛，キャメロット，是亞瑟王傳說中的王國，堅不可摧的城堡。也是亞瑟王朝處於黃金時代的標誌，也是它的政治權利中心和亞瑟王最愛的家園。這座金碧輝煌的城堡令四海英雄皆心嚮往之，無不渴望投奔其中，成為圓桌騎士的一員。）

嘟噥著什麼。

「順便說一下，【威士・奧爾】是特定成員組成的一個小組。」

「啊，是這樣啊……」

感覺這一帶盡可能地追究到底是沒用的。

「那麼，我就算不屬於圓桌會議，但也是【虛擬入部】吧？」

「反正你也會給予【騎士】的稱號吧。請加油！」

「哦，哦。那就是，不努力不行呢……」

總之，就算是假設，似乎也被認定為同伴。

「那麼，你們對我有什麼期待呢？必要的情報是指？」

夏洛特改正了話題，鄭重地告訴她（但是不隱瞞可愛）。

「學院現在被黑暗組織盯上了。」

「哈？」

「像幕後學生會之類的，你知道這種感覺很奇怪的活動嗎？」

「啊……啊，有啊。雖然不知道是不是幕後學生會，但是奇怪的宗教團體不僅在學院，在王國內也有暗中活動。」

「就是那個！說詳細點！」

「雖然很討厭，但是只是在我的興趣範圍以外，從波爾科斯君那裡聽到了老師們的傳言水準而已？」

「沒關係。更詳細地請與麗莎合作，努力收集情報，不過，可以嗎？」

「唔，實在太麻煩了——哎呀，這樣的工作從結果來看對我有好處。讓我們來幹吧。所以麗莎君，請不要那樣瞪我哦。」

因為氣溫又開始下降，提亞利埃塔顫抖著。好像快要凍結了。

「那麼首先請告訴我路塞亞納爾教授所持有的情報。」

「多一些也沒關係。嗯，首先是關於宗教團體的……」

在低溫中進行情報交換。
從頭到尾——。

　
☆☆☆☆☆


我見過濕婆！

上課時，為了防止圓桌會議的成員失控而施行的監視用結界發出的警報，突然響起。

雖然嚇得幾乎要跳起來了，但沒想到是被認為是最不會亂跑的麗莎。
嘛，如果是芙蕾的話，現在的提亞教授已經變成灰了吧。她撿到了一條命。

即使…也…。

「是幕後學生會嗎……」

為了不讓任何人聽到而小聲嘟噥。
雖說不是那種直截了當，但總覺得有異臭味的傢伙在學院暗中活動。
雖然老師們還沒有掌握好實際情況，但這不是很麻煩的事嗎？

總之，情報收集就交給提亞教授們吧，如果有什麼事，我在背後來做點什麼，比較好呢？繼續麗莎們預先監視。

但是總覺得，是那個啊。

濕婆守護會……到底誰在守護著我呢？