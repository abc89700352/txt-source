──鶫背著意識混濁的鈴城，拚命的奔跑著。

「給我、開什麼玩笑啊！！！既然都受了那麼重的創傷，趕緊給我升天啊！！！」

一邊焦躁的抱怨著，一邊警惕著後方傳來的沉重腳步聲。說實話，現在的處境真的十分糟糕。


──時間回到，在鶫和鈴城順利逃出迷宮，鬆懈下來遠遠眺望著熊熊的火焰的時候。

「就算是魔獸，也很難從那個火海中逃脫吧。結界的解除大概也是時間問題了。⋯⋯哦、抱歉吶。如果撐不住的話就先睡會吧？」
「嗯⋯⋯⋯已經是極限了⋯⋯」

鶫擔心的向晃晃悠悠的鈴城提議後，鈴城像斷線的人偶一樣，倒在了鶫的肩旁。看樣子，她比想像中還要更加勉強自己吶。
於是鶫將鈴城背了起來，向著其他人等待的地方趕去。──而就在這時，異變發生了。

突然響起的巨大崩塌聲，以及緊隨而來的是，如同排球大小的黑色火彈。快速掠過鶫的火彈，在鶫的前方炸裂了。這份衝擊甚至劃傷了鶫的臉頰。

鶫不由得回首望去，只見從崩塌的迷宮中，一個黑色的身影正緩緩走來。

──那是，一隻鬼。

比青鬼要小了一圈的身體，然後全身覆蓋著看起來就很堅硬的黑色鎧甲。同時，鎧甲的間隙之間不斷的有黑色火焰溢出，這麼看來，剛才的那個炎彈一定就是這個鬼的傑作吧。

鶫看著這個景象，不由的發出了乾笑。

「哈哈，真的假的啊？」

──不帶這麼玩的吧？

在壬生的交戰中，大致可以判斷出那個青鬼只是一個Ｅ級。一般來說，剛剛的火海已經足以將之燃盡了。所以像這樣又是進化又是復活什麼的，到底誰能預想到啊？

想到這裡，鶫將背上的鈴城穩穩的背好，竭盡全力的奔跑了起來。現在可沒有多餘的時間想對策了，既然那個鬼還活著，那麼鶫現在能做的也就只要背著鈴城趕快逃走了。

但是，在鶫開始逃跑的同時，黑鬼也開始了行動。

──這傢伙，好快！！

黑鬼用著完全不遜色與鶫的速度緊追而來。如果鶫稍微放慢步伐的話，肯定會輕易的被追上吧。再加上黑鬼還有著「炎彈」這一遠程手段，如果稍有大意，很有可能就會被一擊帶走。

鶫猶豫著是否叫醒背上的鈴城，但是馬上便搖頭否決了。她的技能很強力，但是卻有著各種各樣的限制。即使現在使用了她的技能，也很難從那個鬼手上逃脫吧。

這樣一來不是又無計可施了嗎？在不斷的高壓力下，鶫的精神現在也已經接近極限了。不斷擦肩而過的炎彈、已近一點一點被縮短的間距，這一事實不斷的消磨著鶫的神經。在這九死一生的情況下，鶫拚命的分析著逃脫路線。

──不能前往大家的所在地，得找找有障礙物的道路來爭取時間才行。

⋯⋯現在這一刻，鶫完全的捨棄了那位瀕死的魔法少女。在兩位六華都已經無法行動的現在，已經沒有擊退鬼的手段了。現在鶫能做的，只剩下等待那位展開結界的魔法少女失血死亡了，而在這期間，必須盡可能的躲避鬼才行。既然現在已然是存亡之際，當然沒有多餘的精力去顧及她人了。

一邊保護好背上的鈴城，一邊判斷殺意的方向，避開炎彈。但是隨著時間的流逝，身上的傷口也在不斷的增多，腳步也逐漸遲緩，即便如此鶫依舊沒有停下腳步。但是，極限將至。

「──咳、啊。」

不聽使喚的腳被石頭絆倒了，一瞬間，鶫失去了平衡。──而這千載難逢的機會，鬼又怎麼可能會袖手旁觀呢？

黑色炎彈瞄準了鶫的身體，精準的襲來。雖然鶫想要快速的改變體態，但是過度消耗的身體完全跟不上大腦的指令。在無暇顧及之際，鶫迅速的將鈴城抱在了懷中，背對著炎彈，閉上了眼。

──這個位置的話，應該不會致死。所以，就算受些傷，大概還是可以繼續移動吧。

並不是放棄。鶫完全沒有就此死去的打算。於是，鶫為了迎接即將到來的衝擊，要緊了牙關。

但是，疼痛卻遲遲沒有到來。

「⋯⋯誒？」

鶫疑惑的睜開了雙眼，回頭望去。

───

──在哪裡有著一個身著白色斗篷的人物。那女孩戴著的風帽上有著像是兔耳一樣的東西，正在隨風擺動。

現在，她正拿著一個巨大的盾牌，站著鬼的前面，守護著鶫和鈴城。

鶫在一瞬間還以為是瀕死的魔法少女離世，作為替補的魔法少女前來協助了。但是，眼前著不同尋常的既視感很快就否決了鶫的想法。

──鶫知道，「她」是誰。

身體不由得顫抖了起來，眼淚不由自主的從眼簾滑下。嗚咽一樣的悲鳴從鶫的口中漏出。不想相信的事實，卻被映入眼中的真理所否決。

──怎麼會？為什麼啊！明明只有她，只有她──決對不能成為魔法少女啊！！！

鶫用著顫抖的聲音，呼喊道

「──啊、啊啊啊啊！！！『千鳥』！？為什麼會是你啊！！！！！」

聽到呼喊的白衣女孩──千鳥，回過頭，露出了悲傷的笑容。

「⋯⋯對不起，鶫。」


◆　◆　◆

在鶫和鈴城引誘鬼消失在迷宮之時，千鳥雖然感到了無法釋懷，但還是快步趕往了虎杖等人的聚集處。

──雖然好像有什麼理由，但是也不是非得帶上鶫吧。

千鳥真的很擔心鶫。而這並不僅僅是今天，在過去，如果稍微把視線從這個讓人操心的弟弟身上移開，不一會他肯定就會被捲入什麼危機中。

因為從小開始就一直持續著這樣的事情，千鳥在不知不覺間變得比自己想像中還要過保護了。但是，鶫本人卻對自己以身犯險事實全然不知，這導致千鳥為此更是操碎了心。
是因為至今為止的運氣嗎？雖然前不久住過院，但鶫一直以來都沒有受過什麼嚴重的傷。不過，千鳥卻依舊無法消去心中的那份不安。

正因如此，千鳥在回到待機地點，將失去意識的壬生托付給夢路後，毅然決定返程。

在途中，千鳥回想起了之前和芽吹的對話。

──芽吹之前將兩人的關係判斷為「相互依存」

就算去掉兩人作為家人的關係，雙方對於彼此的執著依舊是過於強烈了。而那時芽吹雖然提議去做一下DNA鑑定，明確一下血緣關係，但是千鳥果斷的拒絕了。

雖然並不是懷疑彼此的血緣關係，但是千鳥卻十分害怕
如果，自己和鶫真的不是姐弟的話──。僅僅只是設想，身體便因恐懼而顫抖不已。簡直就如同自己的世界從根源處發生了崩塌一樣的恐懼感。

「惠前輩說、我和鶫的關係是扭曲的。但，即便如此，我也⋯⋯」

──七瀨千鳥，發自真心的愛著七瀨鶫。而這不是作為異性，而是作為家人的愛。但是，如果兩人之間是沒有血緣關係的話，那又會變成什麼樣呢？

當這個大前提崩潰的那一刻，自己又該何去何從呢？這種事情，完全不想去想。
懷著一絲不安，千鳥繼續奔跑著。當她好不容易趕到燃燒的迷宮時，看到了難以置信的光景。

───

「怎麼、會⋯⋯」

在千鳥過來方向的遠處，有著鶫的身影。而他正背著失去意識的鈴城，拚命的奔跑著。然後像是要追趕兩人一樣，一幅黑色姿態的鬼咆哮、奔馳著。

千鳥一邊壓抑住自己的悲鳴，一邊凝視著鶫的身影，呆然的自語了起來。

「⋯⋯難道鈴城小姐失敗了？不對，那個鬼的樣子和之前的青鬼截然不同。難道是新出現的魔獸？」

雖然嘴上這麼說冷靜的分析著，但是千鳥現在能做到的也就只有在在遠處抱著顫抖的身體，袖手旁觀。絲毫不遜色於鶫的腳程，以及目所能及的遠處攻擊手段，那個黑鬼一定比之前那個青鬼更加強大。

──如果，鶫被那個鬼追上了的話。

毫無疑問，鶫必然會斃命吧。如果他將鈴城作為誘餌，或許還有一線生機，但是鶫肯定不會這麼做吧。

「怎、怎麼做，我到底該怎麼做才能幫到鶫呢？」

──現在衝出去，將自己作為誘餌？

不可能。如果真的那麼做的話，鶫一定會以身犯險，直接去挑釁鬼，然後將鬼的注意力再次拽回自己身上。

──喚醒壬生，讓她再次參戰？

即使喚醒她，也很難保證她還有繼續作戰的體力，而且即使現在趕回去，時間上也來不及了。
腦海中迅速閃過的幾個方案，但是全部都有著巨大的缺陷，無法實現。然後，千鳥說出了一個之前一直擱置的方案。

「去借助那位被擊敗的魔法少女的力量，或者是將她⋯⋯」

然後在替補的魔法少女趕來後，一切都會得到解決⋯⋯但是，這也就意味著千鳥的社會性死亡。

不論是何緣故，奪取他人性命都是極刑。而且，傷害魔法少女的罪行要比一般的犯罪行為更加嚴重。從現在魔法少女的社會地位來看，這份特殊對待也是理所當然吧。即使最後可以順利逃離這個絕境，千鳥恐怕再也無法和鶫在一起了吧。

千鳥僅僅握住顫抖上手，閉上了眼睛。在現在煩惱的同時，鶫的性命也岌岌可危。──必須要做出決斷。

「唔、嗚⋯⋯」

伴隨著哽咽，淚水不受控制的從雙眼滑落。明明這是拯救鶫的最後手段了，但是內心深處卻不斷的否決著自己。

無論好壞，千鳥本身依舊是一個善良的人。奪取他人性命的行為，對她來說又怎會輕易為之呢？

───

「──就這麼擔心你的弟弟嗎？」

在哭泣的千鳥身旁，一個聲音響起。無機質，像青年一樣明朗的聲音。千鳥迅速睜開眼睛，環視四周。但是，周圍並沒有人影。

「你是，誰？」

千鳥滿懷戒心的詢問道。「看下面。」聲音再次響起。千鳥戰戰兢兢的向下看去，只見一隻有著金色眼眸的白兔正凝視著自己。

「再問一個問題吧。女孩，你不論發生任何事都會站在弟弟的一方嗎？」

千鳥雖然對這個唐突的問題感到而來疑惑，但是還是肯定的點了點頭。千鳥站在鶫的一方，理所當然的事情。

──但是，為何白兔要問這件事呢？在千鳥開口詢問之前，白兔再次開口了。

「女孩。怎麼稱呼？」
「千、⋯⋯七瀨，千鳥。」

對這一臉困惑的千鳥，白兔繼續淡淡的說道

「是嗎。那麼，千鳥喲，再這樣下去你的弟弟必死無疑。想必你也很清楚吧？」
「才⋯⋯」

對於這句話，千鳥不甘的咬緊了嘴唇。這種事情，即使你不說我也很清楚。而正因如此，千鳥才陷入了猶豫。

──但是，如果還有其他的方法的話。我想要選擇不會傷害他人的方法。

大概是察覺了千鳥的迷惑吧，白兔提出了一個提議。

「──唔姆。那麼，和我締結契約吧。」
「⋯⋯誒？」
「千鳥，就讓我賦予你打破現狀的力量吧。──作為代價，事後你必須實現我的一個『願望』」

白兔眯起了眼睛，將小巧的手爪伸向了千鳥。

「選擇吧。我不會再說第二遍了。」

千鳥睜大了被淚水打濕的眼睛，凝視著白兔。──這是，來自於神的「勸誘」

千鳥現在正站在人生的岔路口上。沒錯──是否要成為魔法少女呢？
雖然聽說過在野的魔法少女，在實現神的願望的同時，也必須回應神的要求。

但是這個願望是什麼，即使現在向它詢問，也一定不會得到答覆吧。這個白兔姿態的神，現在正等待著「是」與「否」兩個回答。如果千鳥選擇否定的話，它一定會像幻影一樣消失吧。

所以，千鳥的回答早已注定。
千鳥一邊想著鶫的反應，一邊緩緩的蹲下。

──他，肯定會生氣吧。

鶫，肯定不允許千鳥成為魔法少女吧。「很危險，所以趕緊給我引退」，說著這樣話語的鶫的形象很輕易的就浮現在了腦海中。
千鳥露出淡淡的微笑，捂住了白兔的手。然後恭敬的底下了頭，說道

「請賦予我力量吧。──我，想要去救鶫。」
「啊。──那麼，契約成立。」

隨著這句活一同，溫暖的輕風在千鳥周圍吹拂。簡直就像是身體的一部分發生了變化一樣，有種不可思議的感覺。千鳥一邊凝視著逐漸變化的服裝，一邊向白兔問道

「神大人。我該如何稱呼您呢？」

白兔若有所思的歪了歪頭。然後，轉過身背對著千鳥，眺望著天空，說道

「也是吶⋯⋯⋯叫我『白』就可以了。──那麼，趕緊做好準備吧。我這邊能給你爭取的時間可並不多吶。」